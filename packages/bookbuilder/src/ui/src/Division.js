import { find, map } from 'lodash'
import config from 'config'
import React from 'react'
import { Draggable, Droppable } from 'react-beautiful-dnd'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import {FormattedMessage, injectIntl} from 'react-intl';


import AddComponentButton from './AddComponentButton'
import BookComponent from './BookComponent'

const DivisionContainer = styled.div`
  counter-reset: component chapter part unnumbered;
  display: flex;
  flex-direction: column;
  margin-bottom: calc(4 * ${th('gridUnit')});
`
const DivisionHeader = styled.span`
  color: ${th('colorPrimary')};
  flex-basis: content;
  font-family: 'Vollkorn';
  font-size: ${th('fontSizeHeading3')};
  font-style: normal;
  font-weight: normal;
  letter-spacing: 5px;
  line-height: ${th('lineHeightHeading3')};
  margin: 0 calc(2 * ${th('gridUnit')}) 0 0;
  padding-top: 5px;
`
const HeaderContainer = styled.div`
  align-items: center;
  display: flex;
  margin-bottom: calc(2 * ${th('gridUnit')});
`
const DivisionActions = styled.div`
  display: flex;
`

const EmptyList = styled.div`
  color: ${th('colorText')};
  font-family: 'Fira Sans';
  font-size: ${th('fontSizeBase')};
  font-style: normal;
  font-weight: normal;
  line-height: ${th('lineHeightBase')};
  margin-left: calc(2 * ${th('gridUnit')});
`
const BookComponentList = styled.div`
  color: ${th('colorText')};
  font-style: normal;
  font-weight: normal;
  z-index: 1;
`
class Division extends React.Component {
  constructor(props) {
    super(props)

    this.onAddClick = this.onAddClick.bind(this)
    this.onRemove = this.onRemove.bind(this)
    this.onUpdatePagination = this.onUpdatePagination.bind(this)
    this.onUpdateWorkflowState = this.onUpdateWorkflowState.bind(this)

  }


  onAddClick(componentType) {
    const { add, bookId, divisionId } = this.props

    add({
      variables: {
        input: {
          title: 'Sans titre',
          bookId,
          componentType,
          divisionId,
        },
      },
    })
  }

  onRemove(bookComponentId) {
    const { deleteBookComponent } = this.props
    deleteBookComponent({
      variables: {
        input: {
          id: bookComponentId,
          deleted: true,
        },
      },
    })
  }

  onUpdatePagination(bookComponentId, pagination) {
    const { updateBookComponentPagination } = this.props
    updateBookComponentPagination({
      variables: {
        input: {
          id: bookComponentId,
          pagination,
        },
      },
    })
  }
  onUpdateWorkflowState(bookComponentId, workflowStates) {
    const { updateBookComponentWorkflowState } = this.props
    const workflowStages = map(workflowStates, item => ({
      label: item.label,
      type: item.type,
      value: item.value,
    }))

    updateBookComponentWorkflowState({
      variables: {
        input: {
          id: bookComponentId,
          workflowStages,
        },
      },
    })
  }

  render() {
    const {
      bookId,
      currentUser,
      updateBookComponentUploading,
      updateBookComponentContent,
      onDeleteBookComponent,
      outerContainer,
      divisionId,
      onWarning,
      showModal,
      onWorkflowUpdate,
      onAdminUnlock,
      showModalToggle,
      history,
      bookComponents,
      label,
      update,
      reorderingAllowed,
      updateComponentType,
      rules,
      intl
    } = this.props

    const { canViewAddComponent } = rules
    const bookComponentInstances = map(bookComponents, (bookComponent, i) => {
      const {
        componentType,
        uploading,
        bookId,
        lock,
        divisionId,
        componentTypeOrder,
        hasContent,
        title,
        id,
        workflowStages,
        pagination,
        trackChangesEnabled,
      } = bookComponent
      return (
        <Draggable key={id} draggableId={id} index={i}>
          {(provided, snapshot) => {
            return (
              <div ref={provided.innerRef} {...provided.draggableProps}>
                <BookComponent
                  bookId={bookId}
                  onAdminUnlock={onAdminUnlock}
                  onWorkflowUpdate={onWorkflowUpdate}
                  currentUser={currentUser}
                  canDrag={reorderingAllowed}
                  componentType={componentType}
                  componentTypeOrder={componentTypeOrder}
                  onDeleteBookComponent={onDeleteBookComponent}
                  divisionId={divisionId}
                  divisionType={label}
                  hasContent={hasContent}
                  history={history}
                  id={id}
                  updateBookComponentContent={updateBookComponentContent}
                  updateBookComponentUploading={updateBookComponentUploading}
                  key={id}
                  lock={lock}
                  no={i}
                  onEndDrag={() => console.log('hello')}
                  onMove={() => console.log('hello')}
                  outerContainer={outerContainer}
                  pagination={pagination}
                  provided={provided}
                  remove={this.onRemove}
                  rules={rules}
                  showModal={showModal}
                  showModalToggle={showModalToggle}
                  title={title}
                  trackChangesEnabled={trackChangesEnabled}
                  updateComponentType={updateComponentType}
                  onWarning={onWarning}
                  updatePagination={this.onUpdatePagination}
                  updateWorkflowState={this.onUpdateWorkflowState}
                  uploading={uploading}
                  update={update}
                  workflowStages={workflowStages}
                />
              </div>
            )
          }}
        </Draggable>
      )
    })
    const divisionsConfig = find(config.bookBuilder.divisions, ['name', label])

    let addButtons = null

    if (canViewAddComponent) {
      addButtons = map(divisionsConfig.allowedComponentTypes, componentType => (
        <AddComponentButton
          add={this.onAddClick}
          label={
            intl.formatMessage(
              {
              id: "app.add_"+componentType,
              defaultMessage: "NO_TRAD_ADD_"+ componentType
            })
          }
          type={componentType}
        />
      ))
    }

    return (
      <DivisionContainer>
        <HeaderContainer>
          <DivisionHeader>
            <FormattedMessage
                id={label}
                defaultMessage= {"NO_TRAD_"+label.toUpperCase()}
            />
          </DivisionHeader>
          <DivisionActions>{addButtons}</DivisionActions>
        </HeaderContainer>
        <Droppable droppableId={divisionId}>
          {(provided, snapshot) => (
            <div
              ref={provided.innerRef}
              style={{
                opacity: snapshot.isDraggingOver ? 0.5 : 1,
                minHeight: '96px',
              }}
            >
              {bookComponents.length > 0 ? (
                <BookComponentList>{bookComponentInstances}</BookComponentList>
              ) : (
                  <EmptyList>
                    <FormattedMessage
                        id="app.empty.division"
                        defaultMessage="There are no items in this division"
                    />
                  </EmptyList>
              )}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DivisionContainer>
    )
  }
}

export default injectIntl(Division)
