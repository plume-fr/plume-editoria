import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const FirstRow = styled.div`
  display: flex;
  margin-bottom: 4px;
`
export default FirstRow
