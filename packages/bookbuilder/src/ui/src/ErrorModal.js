import React from 'react'

import AbstractModal from 'editoria-common/src/AbstractModal'

const ErrorModal = ({ container, show, toggle }) => {
  const body = (
    <div>
      An error occurred during the conversion to epub. Please try again later.
    </div>
  )
  const title = 'Une erreur a eu lieu, n\'hesitez pas à contacter l\'équipe Plume Rédaction'
  return (
    <AbstractModal
      body={body}
      container={container}
      show={show}
      title={title}
      toggle={toggle}
    />
  )
}

export default ErrorModal
