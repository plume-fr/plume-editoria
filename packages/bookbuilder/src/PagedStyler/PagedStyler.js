import React, { Component } from 'react'
import axios from 'axios'
import styled from 'styled-components'
import 'codemirror/mode/css/css'
import 'codemirror/lib/codemirror.css'

const Wrapper = styled.div`
  display: flex;
  align-items: flex-start;
  height: 100%;
  justify-content: flex-start;
  padding: 8px;
`
const Actions = styled.button`
  background: none;
  color: #0d78f2;
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: inherit;
  margin-right: 20px;
`
const PreviewToolbar = styled.div`
  display: flex;
  justify-content: flex-end;
  height: 5%;
`
const PreviewArea = styled.div`
  max-width: 100%;
  width: 100%;
  height: 100%;
  iframe {
    width: 100%;
    height: 100%;
  }
`
class PagedStyler extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
      random: 0,
    }
    this.handlePrint = this.handlePrint.bind(this)
    this.handleDownload = this.handleDownload.bind(this)
  }

  componentDidMount() {
    const { match } = this.props
    const { params } = match
  }

  /* eslint-disable */
  handlePrint(e) {
    const { match } = this.props
    const { params } = match
    const { hashed } = params
    window.open(`/uploads/paged/${hashed}/index.pdf`, "_blank")
  }

  handleDownload(e) {
    e.preventDefault()
    const { match } = this.props
    const { params } = match
    const { hashed } = params
    axios.get(`/api/pagedStyler/exportHTML/${hashed}/`).then(res => {
      window.location.replace(res.request.responseURL)
      // console.log('res', res)
    })
  }

  render() {
    const { match } = this.props
    const { params } = match
    const { hashed } = params

    return (
      <Wrapper>
        <PreviewArea>
          <PreviewToolbar>
            <Actions onClick={this.handlePrint}>Télécharger le PDF</Actions>
            <Actions onClick={this.handleDownload}>Télécharger le fichier HTML</Actions>
          </PreviewToolbar>
          <p>La prévisualiation est temporairement désactivée,</p>
          <p>Vous pouvez télécharger le PDF généré en cliquant sur "Télécharger PDF"</p>
        </PreviewArea>
      </Wrapper>
    )
  }
}

PagedStyler.propTypes = {}

export default PagedStyler
