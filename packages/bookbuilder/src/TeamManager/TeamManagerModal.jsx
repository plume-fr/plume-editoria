import React from 'react'
import PropTypes from 'prop-types'

import CustomModal from 'editoria-common/src/CustomModal'
import TeamManager from './TeamManager'

class TeamManagerModal extends React.Component {
  renderBody() {
    const {
      teams,
      findUser,
      updateTeam,
      // bookId,
      rules,
      canViewAddTeamMember,
      hideModal,
    } = this.props

    return (
      <TeamManager
        // bookId={bookId}
        canViewAddTeamMember={canViewAddTeamMember}
        findUser={findUser}
        hideModal={hideModal}
        rules={rules}
        teams={teams}
        updateTeam={updateTeam}
      />
    )
  }

  render() {
    const { isOpen, hideModal, loading, loadingRules } = this.props
    if (loading || loadingRules) return null
    const body = this.renderBody()

    return (
      <CustomModal
        isOpen={isOpen}
        shouldCloseOnOverlayClick={false}
        size="medium"
        headerText="Acteurs du controle"
        onRequestClose={hideModal}
      >
        {body}
      </CustomModal>
    )
  }
}

TeamManagerModal.propTypes = {
  container: PropTypes.any.isRequired,
  show: PropTypes.bool.isRequired,
  teams: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      type: PropTypes.string,
      rev: PropTypes.string,
      teamType: PropTypes.shape({
        name: PropTypes.string,
        permissions: PropTypes.arrayOf(PropTypes.string),
      }),
      members: PropTypes.arrayOf(PropTypes.string),
      object: PropTypes.shape({
        id: PropTypes.string,
        type: PropTypes.string,
      }),
    }),
  ).isRequired,
  toggle: PropTypes.func.isRequired,
  users: PropTypes.arrayOf(
    PropTypes.shape({
      admin: PropTypes.bool,
      email: PropTypes.string,
      id: PropTypes.string,
      rev: PropTypes.string,
      type: PropTypes.string,
      username: PropTypes.string,
    }),
  ),
  updateTeam: PropTypes.func.isRequired,
}

TeamManagerModal.defaultProps = {
  users: null,
}

export default TeamManagerModal
