import React from 'react'
import styled from 'styled-components'

import {
  ProductionEditorsArea,
  TeamManagerButton,
  Header,
  UploadFilesButton,
  DownloadEpubButton,
  MetadataButton,
  BookExporter,
  DivisionsArea,
} from './ui'

// import ConnectedTeamManager from './TeamManager/ConnectedTeamManager'

// import styles from './styles/bookBuilder.local.scss'

// TODO -- this doesn't work if imported in the css files. why?
// import './styles/fontAwesome.scss'

const Container = styled.div`
  display: block;
  clear: both;
  float: none;
  margin: 0 auto;
  width: 76%;
`
export class BookBuilder extends React.Component {
  constructor(props) {
    super(props)

    // this.toggleTeamManager = this.toggleTeamManager.bind(this)
    this.toggleModal = this.toggleModal.bind(this)

    this.state = {
      outerContainer: this,
      showModal: false,
      // showTeamManager: false,
      uploading: {},
    }
  }

  toggleModal() {
    this.setState({
      showModal: !this.state.showModal,
    })
  }

  render() {
    const {
      book,
      state,
      history,
      addBookComponent,
      onMetadataAdd,
      addBookComponents,
      currentUser,
      deleteBookComponent,
      updateBookComponentPagination,
      updateBookComponentOrder,
      updateBookComponentWorkflowState,
      updateBookComponentUploading,
      updateBookComponentContent,
      updateComponentType,
      onDeleteBookComponent,
      onAdminUnlock,
      exportBook,
      onTeamManager,
      onError,
      onWarning,
      rules,
      loading,
      loadingRules,
      setState,
      refetchingBookBuilderRules,
      onWorkflowUpdate,
    } = this.props

    if (loading || loadingRules) return 'Chargement...'
    if (!book) return null
    const { canViewTeamManager, canViewMultipleFilesUpload } = rules
    const { divisions, productionEditors } = book

    const productionEditorActions = []


    const headerActions = [
      <BookExporter
        book={book}
        history={history}
        htmlToEpub={exportBook}
        onError={onError}
      />,
    ]
    if (canViewTeamManager) {
      headerActions.unshift(
        <TeamManagerButton
          label="Equipe"
          onClick={() => onTeamManager(book.id)}
        />,
      )
    }

    return (
      <Container>
        <Header actions={headerActions} bookTitle={book.title} />
        <DivisionsArea
          addBookComponent={addBookComponent}
          onWorkflowUpdate={onWorkflowUpdate}
          addBookComponents={addBookComponents}
          setState={setState}
          onWarning={onWarning}
          currentUser={currentUser}
          onAdminUnlock={onAdminUnlock}
          history={history}
          bookId={book.id}
          deleteBookComponent={deleteBookComponent}
          onDeleteBookComponent={onDeleteBookComponent}
          divisions={divisions}
          rules={rules}
          updateBookComponentContent={updateBookComponentContent}
          updateBookComponentOrder={updateBookComponentOrder}
          updateBookComponentPagination={updateBookComponentPagination}
          updateBookComponentUploading={updateBookComponentUploading}
          updateBookComponentWorkflowState={updateBookComponentWorkflowState}
          updateComponentType={updateComponentType}
        />
      </Container>
    )
  }
}

export default BookBuilder
