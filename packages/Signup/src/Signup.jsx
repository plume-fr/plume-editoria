import React from 'react'
import { Form } from 'formik'
import { override } from '@pubsweet/ui-toolkit'
import {
  ValidatedFieldFormik,
  CenteredColumn,
  Link,
  H1,
  ErrorText,
  Button,
  TextField,
} from '@pubsweet/ui'
import styled from 'styled-components'
import { isEmpty } from 'lodash'

const FormContainer = styled.div`
  ${override('Login.FormContainer')};
`
const SuccessText = styled.div`
  color: green;
`

const Logo = styled.div`
  ${override('Login.Logo')};
`
const validateEmail = value => {
  let error
  if (!value) {
    error = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    error = 'Invalid email address'
  }
  return error
}

const validateUsername = value => {
  let error
  if (value === 'admin') {
    error = 'Nice try!'
  } else if (value === 'null') {
    error = 'This is not a valid username'
  } else if (!value) {
    error = 'Required'
  }
  return error
}

const validateNames = value => {
  let error
  if (value === 'null') {
    error = 'This is not a valid name'
  } else if (!value) {
    error = 'Required'
  }
  return error
}

const validatePassword = value => {
  let error
  if (!value) {
    error = 'Required'
  } else if (value.length < 8) {
    error = 'Password should be more than 7 characters'
  }
  return error
}
const GivenNameInput = props => (
  <TextField
    data-test-id="givenName"
    label="Prénom"
    {...props}
    placeholder="prénom"
  />
)
const SurnameInput = props => (
  <TextField
    data-test-id="surname"
    label="Nom"
    {...props}
    placeholder="nom"
  />
)
const UsernameInput = props => (
  <TextField
    data-test-id="username"
    label="Identifiant"
    {...props}
    placeholder="username"
  />
)

const EmailInput = props => (
  <TextField
    data-test-id="email"
    label="Email"
    {...props}
    placeholder="email"
    type="email"
  />
)
const PasswordInput = props => (
  <TextField
    data-test-id="password"
    label="Mot de passe"
    {...props}
    placeholder="mot de passe"
    type="password"
  />
)

const Signup = ({
  error,
  errors,
  touched,
  status,
  handleSubmit,
  logo = null,
}) => (
  <div>
    {logo && (
      <Logo>
        <img alt="ccomptes-logo" src="https://upload.wikimedia.org/wikipedia/fr/thumb/3/39/Logo_Cours_des_comptes_%28France%29.svg/1920px-Logo_Cours_des_comptes_%28France%29.svg.png" />
      </Logo>
    )}
    <FormContainer>
      <div className="form">
        {error && <ErrorText>{error}</ErrorText>}
        {status && <SuccessText>Compte créé</SuccessText>}

        <Form onSubmit={handleSubmit}>
          <ValidatedFieldFormik
            component={GivenNameInput}
            name="givenName"
            validate={validateNames}
          />
          <ValidatedFieldFormik
            component={SurnameInput}
            name="surname"
            validate={validateNames}
          />
          <ValidatedFieldFormik
            component={UsernameInput}
            name="username"
            validate={validateUsername}
          />
          <ValidatedFieldFormik
            component={EmailInput}
            name="email"
            validate={validateEmail}
          />
          <ValidatedFieldFormik
            component={PasswordInput}
            name="password"
            validate={validatePassword}
          />
          <Button disabled={error || !isEmpty(errors)} primary type="submit">
            Créer un compte
          </Button>
        </Form>

        <div>
          <Link to="/login">Retour à la page d'authentification</Link>
        </div>
      </div>
    </FormContainer>
  </div>
)

export default Signup
