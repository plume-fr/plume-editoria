import React from 'react';
import { compose } from 'recompose'
import { graphql } from 'react-apollo';
import { UPDATE_BOOK_COMPONENT_CONTENT, GET_BOOK_COMPONENT } from './queries/bookComponentQueries';

export default function withBlocks(WrappedComponent, { match }) {
  const { params } = match;
  const { bookComponentId } = params;

  class Blocks extends React.Component {
     updateContent = ({id, content}) => {
        this.props.mutate({variables:{input: {
          id,
          content: JSON.stringify(content)
          }}})
    }
    render() {
      const { data, error, loading } = this.props;

      if (loading) {
        return <div>loading</div>;
      } 
      if (error) {
        return <p>ERROR</p>
      };
      if (!data) {
        return <p>Not found</p>
      };
      if (data && !data.loading && data.getBookComponent && data.getBookComponent.content) {
        const bookContent = JSON.parse(data.getBookComponent.content);
        return <div>
          <WrappedComponent
            importedDoc={bookContent}
            id={bookComponentId}
            updateContent={this.updateContent}
            saveInterval={5000}
          />
        </div>;
      }
      return <div>loading</div>;
    }
  };
  
  return compose(
    graphql(
      GET_BOOK_COMPONENT,  {
        options: () => ({ variables: { id: bookComponentId } })
      }
    ),
    graphql(
      UPDATE_BOOK_COMPONENT_CONTENT
    )
  )(Blocks);
}
