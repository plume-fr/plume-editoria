import gql from 'graphql-tag';

export const GET_BOOK_COMPONENT = gql`
query GetBookComponent($id: ID!) {
  getBookComponent(id: $id) {
    id
    divisionId
    divisionType
    bookTitle
    title
    bookId
    hasContent
    componentTypeOrder
    componentType
    trackChangesEnabled
    workflowStages {
      label
      type
      value
    }
    lock {
      userId
      username
      created
      givenName
      isAdmin
      surname
    }
    content
  }
}
`;

export const UPDATE_BOOK_COMPONENT_CONTENT = gql`
  mutation UpdateBookComponentContent($input: UpdateBookComponentInput!) {
    updateContent(input: $input) {
      id
      content
    }
  }
`;







