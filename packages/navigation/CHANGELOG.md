# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.5"></a>
## [0.1.5](https://gitlab.coko.foundation/editoria/editoria-navigation/compare/pubsweet-component-editoria-navigation@0.1.4...pubsweet-component-editoria-navigation@0.1.5) (2019-05-28)




**Note:** Version bump only for package pubsweet-component-editoria-navigation

<a name="0.1.4"></a>
## [0.1.4](https://gitlab.coko.foundation/editoria/editoria-navigation/compare/pubsweet-component-editoria-navigation@0.1.3...pubsweet-component-editoria-navigation@0.1.4) (2019-05-17)




**Note:** Version bump only for package pubsweet-component-editoria-navigation

<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/editoria/editoria-navigation/compare/pubsweet-component-editoria-navigation@0.1.2...pubsweet-component-editoria-navigation@0.1.3) (2019-05-17)




**Note:** Version bump only for package pubsweet-component-editoria-navigation

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/editoria/editoria-navigation/compare/pubsweet-component-editoria-navigation@0.1.1...pubsweet-component-editoria-navigation@0.1.2) (2019-04-24)




**Note:** Version bump only for package pubsweet-component-editoria-navigation

<a name="0.1.1"></a>
## 0.1.1 (2019-04-12)


### Bug Fixes

* **navigation:** fix back to book on wax ([9482769](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/9482769))
* **navigation:** fix navigation back to book ([549e50b](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/549e50b))
* **upgrade:** remove redux dependencies upgrade client ([be4d87b](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/be4d87b))
* **wax:** add user color ([c314de5](https://gitlab.coko.foundation/editoria/editoria-navigation/commit/c314de5))




# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.
