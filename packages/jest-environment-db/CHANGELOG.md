# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.0.2"></a>
## [1.0.2](https://gitlab.coko.foundation/editoria/editoria/compare/jest-environment-db@1.0.1...jest-environment-db@1.0.2) (2019-04-12)


### Bug Fixes

* **jest-environment-db:** reinstate package.json ([194fcfd](https://gitlab.coko.foundation/editoria/editoria/commit/194fcfd))
