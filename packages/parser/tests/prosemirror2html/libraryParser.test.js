const data = require('../data/text_exemple')
const Renderer = require("prosemirror-to-html-js").Renderer;
const Table = require('../../prosemirror2html/nodes/Table')

test('translate json to xml', () => {

    const expectedXml =
        '<h1>Les rapports publics de la Cour des comptes</h1>'+
        '<p></p>'+
        '<h2>Titre de niveau 2</h2>'+
        '<h3>Titre de niveau 3</h3>'+
        '<h2>Titre de niveau 2</h2>'+
        '<p></p>'+
        '<p>La <a href="https://www.ccomptes.fr">Cour</a> publie, chaque année, un rapport public annuel et des rapports publics thématiques.</p>' +
        '<p><img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F8%2F88%2FMaurice_Quentin_de_La_Tour_-_Marquise_de_Pompadour_-_WGA12359.jpg%2F132px-Maurice_Quentin_de_La_Tour_-_Marquise_de_Pompadour_-_WGA12359.jpg&f=1&nofb=1"' +
        ' alt="Pompadour"' +
        ' title="Pompadour">' +
        '</p>'+
        '<p></p>'

    const renderer = new Renderer()
    expect(renderer.render(data)).toEqual(expectedXml)
})