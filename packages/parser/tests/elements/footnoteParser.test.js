const FootNoteParser = require('../../elements/footnoteParser')
const BasicParser = require('./../../elements/basicParser')

test('insert footnotes', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html>' +
      '<head></head>' +
      '<body>' +
      '<content attr-id="ee35b36d-85cc-4161-ae79-18a281f47c93" attr-title="Chapitre 1" attr-componenttype="chapter" attr-division="body" attr-number="1">' +
      '<container id="main">' +
      '<h1 class="ct">Chapitre 1</h1>' +
      '<p></p>' +
      '<h1>Titre 1</h1>' +
      '<p>blabalab<note data-id="note-574df7653d9cd7f83a9fc7e6aa733c10" disabled="false">&#x200B;</note>.bulubulu</p>' +
      '<p>bliblbiblbi</p>' +
      '<p>ploplopolpo</p>' +
      '<p>blabalab<span><span><note data-id="note-574df7653d9cd7f83a9fc7e6aa733c11" disabled="false">&#x200B;</note></span></span>.bulubulu</p>' +
      '<p>bliblbiblbi</p>' +
      '<p>ploplopolpo</p>' +
      '</container>' +
      '<div id="notes">' +
      '<note-container id="container-note-574df7653d9cd7f83a9fc7e6aa733c10">' +
      '<p>note de bas de page avec liste : - manger, - lire, - dormir</p>' +
      '</note-container>' +
      '<note-container id="container-note-574df7653d9cd7f83a9fc7e6aa733c11">' +
      '<p>note de bas de page avec liste : - eat, - read, - sleep</p>' +
      '</note-container>' +
      '</div>' +
      '</content>' +
      '<content attr-id="462119ba-4375-4fe4-a92a-a36d968c3a63" attr-title="Chapitre 2" attr-componenttype="chapter" attr-division="body" attr-number="2">' +
      '<container id="main">' +
      '<h1 class="ct">Chapitre 2</h1>' +
      '<recommandation>Tr&#xE9;s important a faire, je le recommande</recommandation>' +
      '<p></p>' +
      '<p>une note ici ?<note data-id="note-26add96ff7cdcb07942507558d8ddff6" disabled="false">&#x200B;</note> une note avant &#xE7;a</p>' +
      '</container>' +
      '<div id="notes">' +
      '<note-container id="container-note-26add96ff7cdcb07942507558d8ddff6">' +
      '<p>yes ?</p>' +
      '<p>no ?</p>' +
      '</note-container>' +
      '</div>' +
      '</content>' +
      '</body>' +
      '</html>',
  )

  const expectedXML =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<content attr-id="ee35b36d-85cc-4161-ae79-18a281f47c93" attr-title="Chapitre 1" attr-componenttype="chapter" attr-division="body" attr-number="1">' +
    '<container id="main">' +
    '<h1 class="ct">Chapitre 1</h1>' +
    '<p></p>' +
    '<h1>Titre 1</h1>' +
    '<p>blabalab<a href="#574df7653d9cd7f83a9fc7e6aa733c10"><sup>1</sup></a>.bulubulu</p>' +
    '<div class="footnote" id="574df7653d9cd7f83a9fc7e6aa733c10">' +
    '<span class="noteNb">1</span>' +
    '<p>note de bas de page avec liste : - manger, - lire, - dormir</p>' +
    '</div>' +
    '<p>bliblbiblbi</p>' +
    '<p>ploplopolpo</p>' +
    '<p>blabalab<span><span><a href="#574df7653d9cd7f83a9fc7e6aa733c11"><sup>2</sup></a></span></span>.bulubulu</p>' +
    '<div class="footnote" id="574df7653d9cd7f83a9fc7e6aa733c11">' +
    '<span class="noteNb">2</span>' +
    '<p>note de bas de page avec liste : - eat, - read, - sleep</p>' +
    '</div>' +
    '<p>bliblbiblbi</p>' +
    '<p>ploplopolpo</p>' +
    '</container>' +
    '</content>' +
    '<content attr-id="462119ba-4375-4fe4-a92a-a36d968c3a63" attr-title="Chapitre 2" attr-componenttype="chapter" attr-division="body" attr-number="2">' +
    '<container id="main">' +
    '<h1 class="ct">Chapitre 2</h1>' +
    '<recommandation>Tr&#xE9;s important a faire, je le recommande</recommandation>' +
    '<p></p>' +
    '<p>une note ici ?<a href="#26add96ff7cdcb07942507558d8ddff6"><sup>3</sup></a> une note avant &#xE7;a</p>' +
    '<div class="footnote" id="26add96ff7cdcb07942507558d8ddff6">' +
    '<span class="noteNb">3</span>' +
    '<p>yes ?</p>' +
    '<p>no ?</p>' +
    '</div>' +
    '</container>' +
    '</content>' +
    '</body>' +
    '</html>'

  const changedXML = FootNoteParser.apply(xmlDoc)
  expect(BasicParser.xmlToTxt(changedXML)).toEqual(expectedXML)
})
