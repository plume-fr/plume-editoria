const ForewordParser = require('../../../elements/ccomptes/forewordParser')
const BasicParser = require('./../../../elements/basicParser')

test('add forevord to book', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html><head></head><body></body></html>',
  )
  const expectedXML =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<section id="forewordSection" class="dedicatedPage">' +
    '<h1 class="unnumbered">foreword</h1>' +
    '<div>' +
    '<p>En application des dispositions des articles L. 143-1 et L. 143-0-2 du code des juridictions financi&#xE8;res, la Cour rend publiques ses observations et ses recommandations, au terme d&#x2019;une proc&#xE9;dure contradictoire qui permet aux repr&#xE9;sentants des organismes et des administrations contr&#xF4;l&#xE9;es, aux autorit&#xE9;s directement concern&#xE9;es, notamment si elles exercent une tutelle, ainsi qu&#x2019;aux personnes &#xE9;ventuellement mises en cause de faire conna&#xEE;tre leur analyse.</p>' +
    '<p>La divulgation pr&#xE9;matur&#xE9;e, par quelque personne que ce soit, des pr&#xE9;sentes observations provisoires, qui conservent un caract&#xE8;re confidentiel jusqu&#x2019;&#xE0; l&#x2019;ach&#xE8;vement de la proc&#xE9;dure contradictoire, porterait atteinte &#xE0; la bonne information des citoyens par la Cour. Elle exposerait en outre &#xE0; des suites judiciaires l&#x2019;auteur de toute divulgation dont la teneur mettrait en cause des personnes morales ou physiques ou porterait atteinte &#xE0; un secret prot&#xE9;g&#xE9; par la loi.</p>' +
    '</div>' +
    '</section>' +
    '</body>' +
    '</html>'
  const changedXML = ForewordParser.apply(xmlDoc)
  expect(BasicParser.xmlToTxt(changedXML)).toEqual(expectedXML)
})
