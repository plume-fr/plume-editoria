const RecommendationParser = require('../../../elements/ccomptes/recommendationParser')
const BasicParser = require('./../../../elements/basicParser')

test('extract recommendation element from document', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html>' +
      '<head></head>' +
      '<body>' +
      '<bookstore>' +
      '<book>' +
      '<h1>Everyday Italian</h1>' +
      '<h1>Everyday German</h1>' +
      '<recommandation>Giada De Laurentiis</recommandation>' +
      '<recommandation>Giada De fynjdd</recommandation>' +
      '<year>2005</year>' +
      '</book>' +
      '</bookstore>' +
      '</body>' +
      '</html>',
  )
  const expectedData = [
    '<recommandation>Giada De Laurentiis</recommandation>',
    '<recommandation>Giada De fynjdd</recommandation>',
  ]
  const extractedData = RecommendationParser.extract(xmlDoc)
  const extractedDataStr = extractedData.map(node => xmlDoc(node).toString())
  expect(extractedDataStr).toEqual(expectedData)
})

test('insert recommendation page ', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html>' +
      '<head></head>' +
      '<body>' +
      '<bookstore>' +
      '<book>' +
      '<h1>Everyday Italian</h1>' +
      '<h1>Everyday German</h1>' +
      '<recommandation>Giada De Laurentiis</recommandation>' +
      '<recommandation>Giada De fynjdd</recommandation>' +
      '<year>2005</year>' +
      '</book>' +
      '</bookstore>' +
      '</body>' +
      '</html>',
  )

  const expectedXML =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<section id="recommendationSection" class="dedicatedPage">' +
    '<h1 class="unnumbered">recommendations</h1>' +
    '<a href="#recommendation-0"><p class="recommendationItem">Giada De Laurentiis</p></a>' +
    '<a href="#recommendation-1"><p class="recommendationItem">Giada De fynjdd</p></a>' +
    '</section>' +
    '<bookstore>' +
    '<book>' +
    '<h1>Everyday Italian</h1>' +
    '<h1>Everyday German</h1>' +
    '<recommandation id="recommendation-0">Giada De Laurentiis</recommandation>' +
    '<recommandation id="recommendation-1">Giada De fynjdd</recommandation>' +
    '<year>2005</year>' +
    '</book>' +
    '</bookstore>' +
    '</body>' +
    '</html>'

  const changedXML = RecommendationParser.apply(xmlDoc)
  expect(BasicParser.xmlToTxt(changedXML)).toEqual(expectedXML)
})

test('insert recommendation page in placeholder', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html>' +
      '<head></head>' +
      '<body>' +
      '<bookstore>' +
      '<book>' +
      '<h1>Everyday Italian</h1>' +
      '<h1>Everyday German</h1>' +
      '<placeholder></placeholder>' +
      '<recommandation>Giada De Laurentiis</recommandation>' +
      '<recommandation>Giada De fynjdd</recommandation>' +
      '<year>2005</year>' +
      '</book>' +
      '</bookstore>' +
      '</body>' +
      '</html>',
  )

  const expectedXML =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<bookstore>' +
    '<book>' +
    '<h1>Everyday Italian</h1>' +
    '<h1>Everyday German</h1>' +
    '<placeholder>' +
    '<section id="recommendationSection" class="dedicatedPage">' +
    '<h1 class="unnumbered">recommendations</h1>' +
    '<a href="#recommendation-0"><p class="recommendationItem">Giada De Laurentiis</p></a>' +
    '<a href="#recommendation-1"><p class="recommendationItem">Giada De fynjdd</p></a>' +
    '</section>' +
    '</placeholder>' +
    '<recommandation id="recommendation-0">Giada De Laurentiis</recommandation>' +
    '<recommandation id="recommendation-1">Giada De fynjdd</recommandation>' +
    '<year>2005</year>' +
    '</book>' +
    '</bookstore>' +
    '</body>' +
    '</html>'

  const changedXML = RecommendationParser.apply(xmlDoc)
  expect(BasicParser.xmlToTxt(changedXML)).toEqual(expectedXML)
})
