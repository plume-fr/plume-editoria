const RopFrontPageDataParser = require('../../../../elements/ccomptes/frontPage/ropFrontPageDataParser')
const BasicParser = require('../../../../elements/basicParser')

test('add titlePage to book', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html><head></head><body><span class="booktitle">BookTitle</span></body></html>',
  )
  const expectedXML =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<section class="titlepage">' +
    '<header>' +
    '<p class="report_type">relev&#xE9; d&apos;observation provisoires</p>' +
    '<p class="law_article">(Article R. 143-7 du code des juridictions financi&#xE8;res)</p>' +
    '<h1 class="booktitle">BookTitle</h1>' +
    '</header>' +
    '<p class="disclaimer">Destin&#xE9; &#xE0; recevoir les remarques des personnes destinataires, le pr&#xE9;sent document est provisoire et confidentiel.</p>' +
    '<p class="footer">' +
    '<span class="adress">13, rue Cambon 75100 PARIS CEDEX 01</span>' +
    '<span class="phone_number">+33 1 42 98 95 00</span>' +
    '<span class="website_url">www.ccomptes.fr</span>' +
    '</p>' +
    '</section>' +
    '</body>' +
    '</html>'
  const changedXML = RopFrontPageDataParser.apply(xmlDoc, 'provisoires')
  expect(BasicParser.xmlToTxt(changedXML)).toEqual(expectedXML)
})