const RodFrontPageDataParser = require('../../../../elements/ccomptes/frontPage/rodFrontPageDataParser')
const BasicParser = require('../../../../elements/basicParser')

test('add titlePage to ROD', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html><head></head><body><span class="booktitle">BookTitle</span></body></html>',
  )
  const expectedXML =
    '<html>' +
      '<head></head>' +
      '<body>' +
      '<section class="titlepage">' +
      '<header>' +
      '<p class="report_type">observations d&#xE9;finitives</p>' +
      '<p class="law_article">(Article R. 143-11 du code des juridictions financi&#xE8;res)</p>' +
      '<h1 class="booktitle">BookTitle</h1>' +
      '</header>' +
      '<p class="disclaimer">Le pr&#xE9;sent document, qui a fait l&#x2019;objet d&#x2019;une contradiction avec les destinataires concern&#xE9;s, a&#xA0;&#xE9;t&#xE9;&#xA0;d&#xE9;lib&#xE9;r&#xE9; par la Cour des comptes, le JJ MMM XXXX.</p>' +
      '<p class="disclaimer_bold">En application de l&#x2019;article L. 143-1 du code des juridictions financi&#xE8;res, la communication de ces observations est une pr&#xE9;rogative de la Cour des comptes, qui a seule comp&#xE9;tence pour arr&#xEA;ter la liste des destinataires.</p>' +
      '<p class="footer">' +
      '<span class="adress">13, rue Cambon 75100 PARIS CEDEX 01</span>' +
      '<span class="phone_number">+33 1 42 98 95 00</span>' +
      '<span class="website_url">www.ccomptes.fr</span>' +
      '</p>' +
      '</section>' +
      '</body>' +
      '</html>'

  const changedXML = RodFrontPageDataParser.apply(xmlDoc)
  expect(BasicParser.xmlToTxt(changedXML)).toEqual(expectedXML)
})