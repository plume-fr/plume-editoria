const PrefaceFrontPageDataParser = require('../../../../elements/ccomptes/frontPage/prefaceFrontPageDataParser')
const BasicParser = require('../../../../elements/basicParser')

test('add titlePage to book', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html><head></head><body><span class="booktitle">BookTitle</span></body></html>',
  )
  const expectedXML =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<section class="prefaceTitlepage">' +
    '<div class="team">' +
    '<p class="chambre">[Numero] chambre</p>' +
    '<p class="section">[Numero] section</p>' +
    '<p class="people">Mme M</p>' +
    '</div>' +
    '<header>' +
    '<p class="report_type">Rapport d&apos;analyse des r&#xE9;ponses</p>' +
    '<h1 class="booktitle">BookTitle</h1>' +
    '</header>' +
    '<div class="disclaimer">' +
    '<p>Document confidentiel destin&#xE9; aux membres de la coll&#xE9;gialit&#xE9; d&#xE9;lib&#xE9;rante</p>' +
    '<p>D&#xE9;pos&#xE9; le [date]</p>' +
    '</div>' +
    '<p class="footer">' +
    '<span class="adress">13, rue Cambon 75100 PARIS CEDEX 01</span>' +
    '<span class="phone_number">+33 1 42 98 95 00</span>' +
    '<span class="website_url">www.ccomptes.fr</span>' +
    '</p>' +
    '</section>' +
    '<span class="booktitle">BookTitle</span>' +
    '</body>' +
    '</html>'
  const changedXML = PrefaceFrontPageDataParser.apply(xmlDoc)
  expect(BasicParser.xmlToTxt(changedXML)).toEqual(expectedXML)
})
