const RarFrontPageDataParser = require('../../../../elements/ccomptes/frontPage/rarFrontPageDataParser')
const BasicParser = require('../../../../elements/basicParser')

test('add titlePage to book', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html><head></head><body><span class="booktitle">BookTitle</span></body></html>',
  )
  const expectedXML =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<section class="titlepage">' +
    '<header>' +
    '<p class="report_type">observations d&#xE9;finitives</p>' +
    '<p class="law_article">(Article R. 143-11 du code des juridictions financi&#xE8;res)</p>' +
    '<h1 class="booktitle">BookTitle</h1>' +
    '</header>' +
    '<p class="subdisclaimer">Le pr&#xE9;sent document, qui a fait l&apos;objet d&apos;une contradiction avec les destinataires concern&#xE9;s, a &#xE9;t&#xE9; d&#xE9;lib&#xE9;r&#xE9; par la Cour des comptes, le JJ MM XXXX</p>' +
    '<p class="disclaimer">En application de l&apos;article L. 143-1 du code des juridictions financi&#xE8;res, la communication de ces observations est une pr&#xE9;rogative de la Cour des comptes, qui a seule comp&#xE9;tence pour arr&#xEA;ter la liste des destinataires</p>' +
    '<p class="footer">' +
    '<span class="adress">13, rue Cambon 75100 PARIS CEDEX 01</span>' +
    '<span class="phone_number">+33 1 42 98 95 00</span>' +
    '<span class="website_url">www.ccomptes.fr</span>' +
    '</p>' +
    '</section>' +
    '</body>' +
    '</html>'

  const changedXML = RarFrontPageDataParser.apply(xmlDoc)
  expect(BasicParser.xmlToTxt(changedXML)).toEqual(expectedXML)
})
