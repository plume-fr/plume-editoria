const RevisionParser = require('../../elements/revisionParser')
const BasicParser = require('./../../elements/basicParser')

test('remove track-changes tag', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html>' +
      '<head></head>' +
      '<body>' +
      '<content>' +
      '<p>A plus court terme, les mesures existantes ' +
      '<track-change username="thot" color="{&quot;addition&quot;:&quot;#0c457d&quot;,&quot;deletion&quot;:&quot;#0c457d&quot;}"  user-id="26442df2-5bd3-40e9-96da-9c78a7df656c" status="delete">d</track-change>' +
      '<track-change username="thot" color="{&quot;addition&quot;:&quot;#0c457d&quot;,&quot;deletion&quot;:&quot;#0c457d&quot;}"  user-id="26442df2-5bd3-40e9-96da-9c78a7df656c" status="add">visant &#xE0; </track-change>' +
      '<track-change username="thot" color="{&quot;addition&quot;:&quot;#0c457d&quot;,&quot;deletion&quot;:&quot;#0c457d&quot;}"  user-id="26442df2-5bd3-40e9-96da-9c78a7df656c" status="delete">&apos;</track-change>accompagne' +
      '<track-change username="thot" color="{&quot;addition&quot;:&quot;#0c457d&quot;,&quot;deletion&quot;:&quot;#0c457d&quot;}"  user-id="26442df2-5bd3-40e9-96da-9c78a7df656c" status="add">r</track-change>' +
      '<track-change username="thot" color="{&quot;addition&quot;:&quot;#0c457d&quot;,&quot;deletion&quot;:&quot;#0c457d&quot;}"  user-id="26442df2-5bd3-40e9-96da-9c78a7df656c" status="delete">ment de</track-change>' +
      '<track-change username="thot" color="{&quot;addition&quot;:&quot;#0c457d&quot;,&quot;deletion&quot;:&quot;#0c457d&quot;}"  user-id="26442df2-5bd3-40e9-96da-9c78a7df656c" status="add"><em>blabla</em></track-change>' +
      '</p>' +
      '</content>' +
      '</body>' +
      '</html>',
  )

  const expectedXML =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<content>' +
    '<p>A plus court terme, les mesures existantes ' +
    '<span class="track-change-add">visant &#xE0; </span>' +
    'accompagne' +
    '<span class="track-change-add">r</span>' +
    '<span class="track-change-add"><em>blabla</em></span>' +
    '</p>' +
    '</content>' +
    '</body>' +
    '</html>'

  const changedXML = RevisionParser.apply(xmlDoc)
  expect(BasicParser.xmlToTxt(changedXML)).toEqual(expectedXML)
})

test('keep deleted revision visible', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html>' +
      '<head></head>' +
      '<body>' +
      '<content>' +
      '<p>A plus court terme, les mesures existantes ' +
      '<track-change username="thot" color="{&quot;addition&quot;:&quot;#0c457d&quot;,&quot;deletion&quot;:&quot;#0c457d&quot;}"  user-id="26442df2-5bd3-40e9-96da-9c78a7df656c" status="delete">d</track-change>' +
      '<track-change username="thot" color="{&quot;addition&quot;:&quot;#0c457d&quot;,&quot;deletion&quot;:&quot;#0c457d&quot;}"  user-id="26442df2-5bd3-40e9-96da-9c78a7df656c" status="add">visant &#xE0; </track-change>' +
      '<track-change username="thot" color="{&quot;addition&quot;:&quot;#0c457d&quot;,&quot;deletion&quot;:&quot;#0c457d&quot;}"  user-id="26442df2-5bd3-40e9-96da-9c78a7df656c" status="delete">&apos;</track-change>accompagne' +
      '<track-change username="thot" color="{&quot;addition&quot;:&quot;#0c457d&quot;,&quot;deletion&quot;:&quot;#0c457d&quot;}"  user-id="26442df2-5bd3-40e9-96da-9c78a7df656c" status="add">r</track-change>' +
      '<track-change username="thot" color="{&quot;addition&quot;:&quot;#0c457d&quot;,&quot;deletion&quot;:&quot;#0c457d&quot;}"  user-id="26442df2-5bd3-40e9-96da-9c78a7df656c" status="delete">ment de</track-change>' +
      '<track-change username="thot" color="{&quot;addition&quot;:&quot;#0c457d&quot;,&quot;deletion&quot;:&quot;#0c457d&quot;}"  user-id="26442df2-5bd3-40e9-96da-9c78a7df656c" status="add"><em>blabla</em></track-change>' +
      '</p>' +
      '</content>' +
      '</body>' +
      '</html>',
  )

  const expectedXML =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<content>' +
    '<p>A plus court terme, les mesures existantes ' +
    '<span class="track-change-delete">d</span>' +
    '<span class="track-change-add">visant &#xE0; </span>' +
    '<span class="track-change-delete">&apos;</span>' +
    'accompagne' +
    '<span class="track-change-add">r</span>' +
    '<span class="track-change-delete">ment de</span>' +
    '<span class="track-change-add"><em>blabla</em></span>' +
    '</p>' +
    '</content>' +
    '</body>' +
    '</html>'

  const changedXML = RevisionParser.apply(xmlDoc, true)
  expect(BasicParser.xmlToTxt(changedXML)).toEqual(expectedXML)
})
