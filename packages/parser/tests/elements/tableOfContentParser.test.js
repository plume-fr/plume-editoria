const TableOfContentParser = require('../../elements/tableOfContentParser')
const BasicParser = require('./../../elements/basicParser')

test('insert tableOfContent page ', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html>' +
      '<head></head>' +
      '<body>' +
      '<bookstore>' +
      '<book>' +
      '<h1>Everyday Italian</h1>' +
      '<h2>Everyday German</h2>' +
      '<h3>Everyday German</h3>' +
      '<h4>Everyday German</h4>' +
      '<h2>Everyday German</h2>' +
      '<h1>Everyday German</h1>' +
      '<h2>Everyday German</h2>' +
      '<h2 class="unnumbered">Everyday German</h2>' +
      '<year>2005</year>' +
      '</book>' +
      '</bookstore>' +
      '</body>' +
      '</html>',
  )

  const expectedXML =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<section id="tableOfContentSection" class="dedicatedPage">' +
    '<h1 class="unnumbered">table of content</h1>' +
    '<ol>' +
    '<li class="headingItem level-H1"><a href="#heading-0"><span class="headingNb">1</span><span class="titleTxt">Everyday Italian</span></a></li>' +
    '<li class="headingItem level-H2"><a href="#heading-1"><span class="headingNb">1.1</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H3"><a href="#heading-2"><span class="headingNb">1.1.1</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H4"><a href="#heading-3"><span class="headingNb">1.1.1.1</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H2"><a href="#heading-4"><span class="headingNb">1.2</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H1"><a href="#heading-5"><span class="headingNb">2</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H2"><a href="#heading-6"><span class="headingNb">2.1</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H2"><a href="#heading-7">Everyday German</a></li>' +
    '</ol>' +
    '</section>' +
    '<bookstore>' +
    '<book>' +
    '<h1 id="heading-0"><span class="headingNb">1</span><span class="titleTxt">Everyday Italian</span></h1>' +
    '<h2 id="heading-1"><span class="headingNb">1.1</span><span class="titleTxt">Everyday German</span></h2>' +
    '<h3 id="heading-2"><span class="headingNb">1.1.1</span><span class="titleTxt">Everyday German</span></h3>' +
    '<h4 id="heading-3"><span class="headingNb">1.1.1.1</span><span class="titleTxt">Everyday German</span></h4>' +
    '<h2 id="heading-4"><span class="headingNb">1.2</span><span class="titleTxt">Everyday German</span></h2>' +
    '<h1 id="heading-5"><span class="headingNb">2</span><span class="titleTxt">Everyday German</span></h1>' +
    '<h2 id="heading-6"><span class="headingNb">2.1</span><span class="titleTxt">Everyday German</span></h2>' +
    '<h2 class="unnumbered" id="heading-7">Everyday German</h2>' +
    '<year>2005</year>' +
    '</book>' +
    '</bookstore>' +
    '</body>' +
    '</html>'

  const changedXML = TableOfContentParser.apply(xmlDoc)
  expect(BasicParser.xmlToTxt(changedXML)).toEqual(expectedXML)
})

test('insert tableOfContent page in placeholder ', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html>' +
      '<head></head>' +
      '<body>' +
      '<bookstore>' +
      '<table-of-contents></table-of-contents>' +
      '<book>' +
      '<h1>Everyday Italian</h1>' +
      '<h2>Everyday German</h2>' +
      '<h3>Everyday German</h3>' +
      '<h4>Everyday German</h4>' +
      '<h2>Everyday German</h2>' +
      '<h1>Everyday German</h1>' +
      '<h2>Everyday German</h2>' +
      '<h2 class="unnumbered">Everyday German</h2>' +
      '<year>2005</year>' +
      '</book>' +
      '</bookstore>' +
      '</body>' +
      '</html>',
  )

  const expectedXML =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<bookstore>' +
    '<table-of-contents>' +
    '<section id="tableOfContentSection" class="dedicatedPage">' +
    '<h1 class="unnumbered">table of content</h1>' +
    '<ol>' +
    '<li class="headingItem level-H1"><a href="#heading-0"><span class="headingNb">1</span><span class="titleTxt">Everyday Italian</span></a></li>' +
    '<li class="headingItem level-H2"><a href="#heading-1"><span class="headingNb">1.1</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H3"><a href="#heading-2"><span class="headingNb">1.1.1</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H4"><a href="#heading-3"><span class="headingNb">1.1.1.1</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H2"><a href="#heading-4"><span class="headingNb">1.2</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H1"><a href="#heading-5"><span class="headingNb">2</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H2"><a href="#heading-6"><span class="headingNb">2.1</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H2"><a href="#heading-7">Everyday German</a></li>' +
    '</ol>' +
    '</section>' +
    '</table-of-contents>' +
    '<book>' +
    '<h1 id="heading-0"><span class="headingNb">1</span><span class="titleTxt">Everyday Italian</span></h1>' +
    '<h2 id="heading-1"><span class="headingNb">1.1</span><span class="titleTxt">Everyday German</span></h2>' +
    '<h3 id="heading-2"><span class="headingNb">1.1.1</span><span class="titleTxt">Everyday German</span></h3>' +
    '<h4 id="heading-3"><span class="headingNb">1.1.1.1</span><span class="titleTxt">Everyday German</span></h4>' +
    '<h2 id="heading-4"><span class="headingNb">1.2</span><span class="titleTxt">Everyday German</span></h2>' +
    '<h1 id="heading-5"><span class="headingNb">2</span><span class="titleTxt">Everyday German</span></h1>' +
    '<h2 id="heading-6"><span class="headingNb">2.1</span><span class="titleTxt">Everyday German</span></h2>' +
    '<h2 class="unnumbered" id="heading-7">Everyday German</h2>' +
    '<year>2005</year>' +
    '</book>' +
    '</bookstore>' +
    '</body>' +
    '</html>'

  const changedXML = TableOfContentParser.apply(xmlDoc)
  expect(BasicParser.xmlToTxt(changedXML)).toEqual(expectedXML)
})
