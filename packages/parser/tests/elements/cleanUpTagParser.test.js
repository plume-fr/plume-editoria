const CleanUpTagParser = require('../../elements/cleanUpTagParser')

const BasicParser = require('./../../elements/basicParser')

test('replace tag by proper html', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html>' +
      '<head></head>' +
      '<body>' +
      '<bookstore>' +
      '<book>' +
      '<h1 data-id="dzudziefiezjfiezjiez">Everyday Italian</h1>' +
      '<h1>Everyday German</h1>' +
      '<chapter-title>Title Of the chapter</chapter-title>' +
      '<author>Giada De Laurentiis</author>' +
      '<year>2005</year>' +
      '</book></bookstore>' +
      '</body>' +
      '</html>',
  )

  const expectedXml =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<bookstore>' +
    '<book>' +
    '<h1>Everyday Italian</h1>' +
    '<h1>Everyday German</h1>' +
    '<h1 class="ct">Title Of the chapter</h1>' +
    '<p class="author">Giada De Laurentiis</p>' +
    '<year>2005</year>' +
    '</book></bookstore>' +
    '</body>' +
    '</html>'

  const changedXml = CleanUpTagParser.apply(xmlDoc)
  expect(BasicParser.xmlToTxt(changedXml)).toEqual(expectedXml)
})

test('remove empty or whitespace-only tags inside of body', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html>' +
    '<head></head>' + // is empty but outside of body so shouldn't be removed
    '<body>' +
    '<empty></empty>' +
    '<empty class="empty_with_attribute"></empty>' +
    '<empty class="empty_but_for_space">   </empty>' +
    '<img class="this_tag_is_empty_but_stay_because_img" src="image.png">' + // self closing tag souldn't be removed
    '<h1><span>John</span> <span>Doe</span></h1>' + // space between the span shouldn't be removed
      '</body>' +
      '</html>',
  )

  const expectedXml =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<img class="this_tag_is_empty_but_stay_because_img" src="image.png">' +
    '<h1><span>John</span> <span>Doe</span></h1>' +
    '</body>' +
    '</html>'

  const changedXml = CleanUpTagParser.apply(xmlDoc)
  expect(BasicParser.xmlToTxt(changedXml)).toEqual(expectedXml)
})

test('detect indivisible space', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<span>50 000</span>' + // between part of a number
    '<span>&#xAB; Wonderfull &#xBB;, she said </span>' + // after and before punctuation like << >>
    '<span>Article R. 142-22 and Article L. 546-1-1</span>' + // when refering to a french law
    '<span>Do you need something ?</span>' + // before a ? or ! (yes it's strange but in french we use space before this punctuation ^^)
      '</body>' +
      '</html>',
  )

  // note : the replacement is made with the html code "&nbsp;" but for the deep equality test it needs to use the hexa code : "&#xA0;"

  const expectedXml =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<span>50&#xA0;000</span>' +
    '<span>&#xAB;&#xA0;Wonderfull&#xA0;&#xBB;, she said </span>' +
    '<span>Article&#xA0;R.&#xA0;142-22 and Article&#xA0;L.&#xA0;546-1-1</span>' +
    '<span>Do you need something&#xA0;?</span>' +
    '</body>' +
    '</html>'

  const changedXml = CleanUpTagParser.apply(xmlDoc)
  expect(BasicParser.xmlToTxt(changedXml)).toEqual(expectedXml)
})

test('remove empty or whitespace-only tags inside of body except in a table cell', () => {
  const xmlDoc = BasicParser.txtToXml(
    '<html>' +
    '<head></head>' + // is empty but outside of body so shouldn't be removed
    '<body>' +
    '<empty class="empty_but_for_space">   </empty>' +
    '<table>' +
    '<tbody>' +
    '<tr>' +
    '<td> </td>' +
    '<td> </td>' + // empty cell should not be removed
      '</tr>' +
      '</tbody>' +
      '</table>' +
      '</body>' +
      '</html>',
  )

  const expectedXml =
    '<html>' +
    '<head></head>' + // is empty but outside of body so shouldn't be removed
    '<body>' +
    '<table>' +
    '<tbody>' +
    '<tr>' +
    '<td> </td>' +
    '<td> </td>' + // empty cell should not be removed
    '</tr>' +
    '</tbody>' +
    '</table>' +
    '</body>' +
    '</html>'

  const changedXml = CleanUpTagParser.apply(xmlDoc)
  expect(BasicParser.xmlToTxt(changedXml)).toEqual(expectedXml)
})
