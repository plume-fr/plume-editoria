const { RIOPParser } = require('../../../parsers/plume/riop')
const BasicParser = require('./../../../elements/basicParser')

test('convert to RIOP ', () => {
  const xml = BasicParser.txtToXml(
    '<html>' +
      '<head></head>' +
      '<body>' +
      '<span class="booktitle">BookTitle</span>' +
      '<bookstore>' +
      '<placeholder></placeholder>' +
      '<book>' +
      '<h1>Everyday Italian</h1>' +
      '<h2>Everyday German</h2>' +
      '<h3>Everyday German</h3>' +
      '<h4>Everyday German</h4>' +
      '<h2>Everyday German</h2>' +
      '<h1>Everyday German</h1>' +
      '<h2>Everyday German</h2>' +
      '<recommandation>Giada De Laurentiis</recommandation>' +
      '<recommandation>Giada De fynjdd</recommandation>' +
      '<chapter-title>Title Of the chapter</chapter-title>' +
      '<p>blabalab<note data-id="note-574df7653d9cd7f83a9fc7e6aa733c10" disabled="false">&#x200B;</note>.bulubulu</p>' +
      '<author>Giada De Laurentiis</author>' +
      '<div id="notes">' +
      '<note-container id="container-note-574df7653d9cd7f83a9fc7e6aa733c10">' +
      '<p>note de bas de page avec liste : - manger, - lire, - dormir</p>' +
      '</note-container>' +
      '</div>' +
      '<year>2005</year>' +
      '</book>' +
      '</bookstore>' +
      '</body>' +
      '</html>',
  )

  const expectedXML =
    '<html>' +
    '<head></head>' +
    '<body>' +
    '<section class="titlepage">' +
    '<header>' +
    '<p class="report_type">relev&#xE9; d&apos;observation provisoires</p>' +
    '<p class="law_article">(Article R. 143-7 du code des juridictions financi&#xE8;res)</p>' +
    '<h1 class="booktitle">BookTitle</h1>' +
    '</header>' +
    '<p class="disclaimer">Destin&#xE9; &#xE0; recevoir les remarques des personnes destinataires, le pr&#xE9;sent document est provisoire et confidentiel.</p>' +
    '<p class="footer">' +
    '<span class="adress">13, rue Cambon 75100 PARIS CEDEX 01</span>' +
    '<span class="phone_number">+33 1 42 98 95 00</span>' +
    '<span class="website_url">www.ccomptes.fr</span>' +
    '</p>' +
    '</section>' +
    '<section id="forewordSection" class="dedicatedPage">' +
    '<h1 class="unnumbered">foreword</h1>' +
    '<div>' +
    '<p>En application des dispositions des articles L. 143-1 et L. 143-0-2 du code des juridictions financi&#xE8;res, la Cour rend publiques ses observations et ses recommandations, au terme d&#x2019;une proc&#xE9;dure contradictoire qui permet aux repr&#xE9;sentants des organismes et des administrations contr&#xF4;l&#xE9;es, aux autorit&#xE9;s directement concern&#xE9;es, notamment si elles exercent une tutelle, ainsi qu&#x2019;aux personnes &#xE9;ventuellement mises en cause de faire conna&#xEE;tre leur analyse.</p>' +
    '<p>La divulgation pr&#xE9;matur&#xE9;e, par quelque personne que ce soit, des pr&#xE9;sentes observations provisoires, qui conservent un caract&#xE8;re confidentiel jusqu&#x2019;&#xE0; l&#x2019;ach&#xE8;vement de la proc&#xE9;dure contradictoire, porterait atteinte &#xE0; la bonne information des citoyens par la Cour. Elle exposerait en outre &#xE0; des suites judiciaires l&#x2019;auteur de toute divulgation dont la teneur mettrait en cause des personnes morales ou physiques ou porterait atteinte &#xE0; un secret prot&#xE9;g&#xE9; par la loi.</p>' +
    '</div>' +
    '</section>' +
    '<section id="tableOfContentSection" class="dedicatedPage">' +
    '<h1 class="unnumbered">table of content</h1>' +
    '<ol>' +
    '<li class="headingItem level-H1"><a href="#heading-0">recommendations</a></li>' +
    '<li class="headingItem level-H1"><a href="#heading-1"><span class="headingNb">1</span><span class="titleTxt">Everyday Italian</span></a></li>' +
    '<li class="headingItem level-H2"><a href="#heading-2"><span class="headingNb">1.1</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H3"><a href="#heading-3"><span class="headingNb">1.1.1</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H4"><a href="#heading-4"><span class="headingNb">1.1.1.1</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H2"><a href="#heading-5"><span class="headingNb">1.2</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H1"><a href="#heading-6"><span class="headingNb">2</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H2"><a href="#heading-7"><span class="headingNb">2.1</span><span class="titleTxt">Everyday German</span></a></li>' +
    '<li class="headingItem level-H1"><a href="#heading-8"><span class="headingNb">3</span><span class="titleTxt">Title Of the chapter</span></a></li>' +
    '</ol>' +
    '</section>' +
    '<bookstore>' +
    '<placeholder>' +
    '<section id="recommendationSection" class="dedicatedPage">' +
    '<h1 class="unnumbered" id="heading-0">recommendations</h1>' +
    '<a href="#recommendation-0"><p class="recommendationItem">Giada De Laurentiis</p></a>' +
    '<a href="#recommendation-1"><p class="recommendationItem">Giada De fynjdd</p></a>' +
    '</section>' +
    '</placeholder>' +
    '<book>' +
    '<h1 id="heading-1"><span class="headingNb">1</span><span class="titleTxt">Everyday Italian</span></h1>' +
    '<h2 id="heading-2"><span class="headingNb">1.1</span><span class="titleTxt">Everyday German</span></h2>' +
    '<h3 id="heading-3"><span class="headingNb">1.1.1</span><span class="titleTxt">Everyday German</span></h3>' +
    '<h4 id="heading-4"><span class="headingNb">1.1.1.1</span><span class="titleTxt">Everyday German</span></h4>' +
    '<h2 id="heading-5"><span class="headingNb">1.2</span><span class="titleTxt">Everyday German</span></h2>' +
    '<h1 id="heading-6"><span class="headingNb">2</span><span class="titleTxt">Everyday German</span></h1>' +
    '<h2 id="heading-7"><span class="headingNb">2.1</span><span class="titleTxt">Everyday German</span></h2>' +
    '<recommandation id="recommendation-0">Giada De Laurentiis</recommandation>' +
    '<recommandation id="recommendation-1">Giada De fynjdd</recommandation>' +
    '<h1 class="ct" id="heading-8"><span class="headingNb">3</span><span class="titleTxt">Title Of the chapter</span></h1>' +
    '<p>blabalab<a href="#574df7653d9cd7f83a9fc7e6aa733c10"><sup>1</sup></a>.bulubulu</p>' +
    '<div class="footnote" id="574df7653d9cd7f83a9fc7e6aa733c10"><span class="noteNb">1</span><p>note de bas de page avec liste : - manger, - lire, - dormir</p></div>' +
    '<p class="author">Giada De Laurentiis</p>' +
    '<year>2005</year>' +
    '</book>' +
    '</bookstore>' +
    '</body>' +
    '</html>'

  const changedXML = BasicParser.xmlToTxt(RIOPParser(xml))
  expect(changedXML).toEqual(expectedXML)
})
