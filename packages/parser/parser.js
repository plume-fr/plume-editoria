const { RIOPParser } = require('./parsers/plume/riop')
const { RARParser } = require('./parsers/plume/rar')
const { RODParser } = require('./parsers/plume/rod')
const BasicParser = require('./elements/basicParser')

function parse(xmlAsTxt, style) {
  let xmlDocument = BasicParser.txtToXml(xmlAsTxt)
  switch (style) {
    case 'riop':
      xmlDocument = RIOPParser(xmlDocument)
      break
    case 'rar':
      xmlDocument = RARParser(xmlDocument)
      break
    case 'rar_revision':
      xmlDocument = RARParser(xmlDocument, true)
      break
    case 'rod':
      xmlDocument = RODParser(xmlDocument)
      break
    default:
      xmlDocument = RIOPParser(xmlDocument)
      break
  }

  return BasicParser.xmlToTxt(xmlDocument)
}

module.exports = { parse }
