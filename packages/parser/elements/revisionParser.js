const BasicParser = require('./basicParser')

class RevisionParser extends BasicParser {
  static apply(xmlDocument, deletedRevisionsVisibles) {
    // remove part deleted by revision
    let changedXmlDoc
    if (deletedRevisionsVisibles) {
      changedXmlDoc = RevisionParser.replace(
        xmlDocument,
        'track-change[status="delete"]',
        'span',
        'track-change-delete',
      )
    } else {
      changedXmlDoc = RevisionParser.remove(
        xmlDocument,
        'track-change[status="delete"]',
      )
    }
    changedXmlDoc = RevisionParser.replace(
      changedXmlDoc,
      'track-change[status="add"]',
      'span',
      'track-change-add',
    )
    return changedXmlDoc
  }
}

module.exports = RevisionParser
