const cheerio = require('cheerio')

class basicParser {
  // base of all Parsers.
  // each Parser will have at least an apply function taking and
  // returning a xmlDocument (document as defined by the cheerio library : https://cheerio.js.org/)

  static apply(xmlDocument) {
    return xmlDocument
  }

  static txtToXml(txt) {
    return cheerio.load(txt)
  }

  static xmlToTxt(xml) {
    return xml.xml()
  }

  static replace(xmlDocument, selector, newTag, className) {
    const selectedElts = Array.from(xmlDocument(selector)) // create an array from the Set object to use map, reverse etc..

    const array = Array.from(selectedElts)
    // use reverse because each replaced node is removed from the list and it would break a forward map
    array.reverse().map(elt => {
      const replacement = `<${newTag} class=${className}>${xmlDocument(
        elt,
      ).html()}</${newTag}>`
      xmlDocument(elt).replaceWith(replacement)
    })
    return xmlDocument
  }

  static remove(xmlDocument, selector) {
    xmlDocument(selector).remove()
    return xmlDocument
  }

  static removeAttr(xmlDocument, attr, except) {
    xmlDocument(`[${attr}]:not(${except})`).removeAttr(attr)
    return xmlDocument
  }
}

module.exports = basicParser
