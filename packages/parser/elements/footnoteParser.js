const BasicParser = require('./basicParser')

class FootNoteParser extends BasicParser {
  static apply(xmlDocument) {
    const extractedData = FootNoteParser.extract(xmlDocument)
    return FootNoteParser.handleExtractedData(extractedData, xmlDocument)
  }

  static extract(xmlDocument) {
    const mentionedFootnote = xmlDocument('note-container')
    let footnote
    const extractedData = []
    for (let i = 0; i < mentionedFootnote.length; i++) {
      footnote = xmlDocument(mentionedFootnote[i])
      extractedData.push(footnote.clone())
      footnote.remove()
    }
    xmlDocument('#notes').remove()
    return extractedData
  }

  static handleExtractedData(extractedData, xmlDocument) {
    for (let i = 0; i < extractedData.length; i++) {
      const noteCnt = i + 1
      const footnote = xmlDocument(extractedData[i])

      // initial id format is "container-note-574df7653d9cd7f83a9fc7e6aa733c10"
      const footnoteId = footnote
        .attr('id')
        .split('container-note-')
        .pop()

      // initial ref format is "<note data-id="note-574df7653d9cd7f83a9fc7e6aa733c10""
      const footnoteRef = xmlDocument(`note[data-id="note-${footnoteId}"]`)

      // find non span parent (p, div) so that the div block is not inserted inside a sentence which would cause unwanted line break
      const parent = FootNoteParser.getNearestNonSpanParent(footnoteRef)
      parent.after(
        `<div class="footnote" id="${footnoteId}">` +
          `<span class="noteNb">${noteCnt}</span>` +
          `${footnote.html()}` +
          `</div>`,
      )
      footnoteRef.replaceWith(
        `<a href="#${footnoteId}"><sup>${noteCnt}</sup></a>`,
      )
    }
    return xmlDocument
  }

  static getNearestNonSpanParent(elt) {
    let parent = elt.parent()
    while (parent.is('span')) {
      parent = parent.parent()
    }
    return parent
  }
}

module.exports = FootNoteParser
