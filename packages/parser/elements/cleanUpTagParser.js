const BasicParser = require('./basicParser');

class CleanUpTagParser extends BasicParser {
  static apply(xmlDocument) {
    let changedXmlDoc = CleanUpTagParser.replace(xmlDocument, 'chapter-title', 'h1', 'ct');

        // replace with paragraph
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'chapter-subtitle', 'p', 'cst');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'bibliography-entry', 'p', 'bibliography-entry');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'glossary', 'p', 'glossary');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'author', 'p', 'author');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'half-title', 'p', 'half-title');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'publisher', 'p', 'publisher');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'dedication', 'p', 'dedication');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'signature', 'p', 'signature');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'series-editor', 'p', 'series-editor');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'series-title', 'p', 'series-title');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'source-note', 'p', 'exsn');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'dedication', 'p', 'dedication');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'dedication', 'p', 'dedication');

        // replace with blockquote
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'extract', 'blockquote', 'ex');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'extract-prose', 'blockquote', 'ex');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'extract-poetry', 'blockquote', 'px');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'epigraph-poetry', 'blockquote', 'sepo');
    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'epigraph-prose', 'blockquote', 'sep');

    changedXmlDoc = CleanUpTagParser.replace(changedXmlDoc, 'comment', 'span', 'comment');

        // remove unused block
    changedXmlDoc = CleanUpTagParser.remove(changedXmlDoc, 'container[id*="caption-image"]');
    changedXmlDoc = CleanUpTagParser.removeEmptyTags(changedXmlDoc);

    // remove data-id for readibility of the final html, make an exception for note
    // (they use data-id to identified which note are associated to which footnote)
    changedXmlDoc = CleanUpTagParser.removeAttr(changedXmlDoc, 'data-id', 'note');

    changedXmlDoc = CleanUpTagParser.insertIndivisibleSpaceWhenRelevant(changedXmlDoc);


    return changedXmlDoc
  }

  static removeEmptyTags(xmlDocument) {
    const tableRelatedSelectors = 'td, th, tr, table, tbody, thead'
    xmlDocument(
      `body *:not(img, placeholder, table-of-contents,${tableRelatedSelectors})`,
    )
      .filter(
        // contain nothing or only white space == content is null after trimming
        (i, el) =>
          !xmlDocument(el)
            .text()
            .trim(),
      )
      .remove()
    return xmlDocument
  }

  /* replace :
    << hey >> -> <<&nbsp;hey&nbsp;>>
    50 000 -> 50&nbsp;000
    Article R. 123 -> Article&nbsp;R.&nbsp;123
    Article L. 123 -> Article&nbsp;L.&nbsp;123
    ( what ) -> (&nbsp;what&nbsp;)
    Do you need it ? -> Do you need it&nbsp;?
    Yes we do ! -> Yes we do&nbsp;§
  */
  static insertIndivisibleSpaceWhenRelevant(xmlDocument) {
    let text = CleanUpTagParser.xmlToTxt(xmlDocument)
    text = text.replace(
      /&#xAB; | &#xBB;|[0-9] [0-9]|Article (R|L). [0-9]|\( | \)| \?| !/g,
      x => x.replace(/ /g, '&nbsp;'),
    )
    return CleanUpTagParser.txtToXml(text)
  }
}

module.exports = CleanUpTagParser;
