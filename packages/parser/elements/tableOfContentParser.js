const BasicParser = require('./basicParser')
const { format } = require('editoria-i18n')

class TableOfContentParser extends BasicParser {
  static apply(xmlDocument) {
    xmlDocument = TableOfContentParser.addHeadingNumberingAndId(xmlDocument)
    const extractedHeadings = TableOfContentParser.extractHeadings(xmlDocument)
    return TableOfContentParser.addTableOfContent(
      extractedHeadings,
      xmlDocument,
    )
  }

  static addHeadingNumberingAndId(xmlDocument) {
    const mentionedHeadings = xmlDocument('h1, h2, h3, h4, h5')
    let counters = { l1: 0, l2: 0, l3: 0, l4: 0, l5: 0 }
    for (let i = 0; i < mentionedHeadings.length; i++) {
      const heading = xmlDocument(mentionedHeadings[i])
      heading.attr('id', `heading-${i}`)
      if (!heading.hasClass('unnumbered')) {
        const headingLevel = `level-${heading.prop('tagName')}`
        const numberingObj = TableOfContentParser.getHeadingNumber(
          counters,
          headingLevel,
        )
        counters = numberingObj.newCounters
        heading.html(
          `<span class="headingNb">${
            numberingObj.titleNumbering
          }</span><span class="titleTxt">${heading.html()}</span>`,
        )
      }
    }
    return xmlDocument
  }

  static extractHeadings(xmlDocument) {
    const mentionedHeadings = xmlDocument('h1, h2, h3, h4, h5')
    const extractedData = []
    for (let i = 0; i < mentionedHeadings.length; i++) {
      const heading = xmlDocument(mentionedHeadings[i]).clone()
      extractedData.push(heading)
    }
    return extractedData
  }

  static addTableOfContent(headings, xmlDocument) {
    const sectionId = 'tableOfContentSection'
    const sectionTitle = format('TABLE_OF_CONTENT')
    const place =
      xmlDocument('table-of-contents').length !== 0
        ? xmlDocument('table-of-contents:first-of-type')
        : xmlDocument('body')
    place.prepend(
      `<section id="${sectionId}" class="dedicatedPage">` +
        `<h1 class="unnumbered">${sectionTitle}</h1>` +
        `<ol></ol>` +
        `</section>`,
    )
    for (let i = 0; i < headings.length; i++) {
      const heading = xmlDocument(headings[i])
      const headingId = heading.attr('id')
      heading.removeAttr('id')
      const headingLevel = `level-${heading.prop('tagName')}`
      xmlDocument(`#${sectionId} ol`).append(
        `<li class="headingItem ${headingLevel}"><a href="#${headingId}">${heading.html()}</a></li>`,
      )
    }
    return xmlDocument
  }

  static getHeadingNumber(counters, level) {
    let titleNumbering = ''
    let newCounters = counters
    switch (level) {
      case 'level-H1':
        newCounters = { l1: counters.l1 + 1, l2: 0, l3: 0, l4: 0, l5: 0 }
        titleNumbering = `${newCounters.l1}`
        break
      case 'level-H2':
        newCounters = {
          l1: counters.l1,
          l2: counters.l2 + 1,
          l3: 0,
          l4: 0,
          l5: 0,
        }
        titleNumbering = `${newCounters.l1}.${newCounters.l2}`
        break
      case 'level-H3':
        newCounters = {
          l1: counters.l1,
          l2: counters.l2,
          l3: counters.l3 + 1,
          l4: 0,
          l5: 0,
        }
        titleNumbering = `${newCounters.l1}.${newCounters.l2}.${newCounters.l3}`
        break
      case 'level-H4':
        newCounters = {
          l1: counters.l1,
          l2: counters.l2,
          l3: counters.l3,
          l4: counters.l4 + 1,
          l5: 0,
        }
        titleNumbering = `${newCounters.l1}.${newCounters.l2}.${
          newCounters.l3
        }.${newCounters.l4}`
        break
      case 'level-H5':
        newCounters = {
          l1: counters.l1,
          l2: counters.l2,
          l3: counters.l3,
          l4: counters.l4,
          l5: counters.l5 + 1,
        }
        titleNumbering = `${newCounters.l1}.${newCounters.l2}.${
          newCounters.l3
        }.${newCounters.l4}.${newCounters.l5}`
        break
      default:
        break
    }
    return { titleNumbering, newCounters }
  }
}

module.exports = TableOfContentParser
