const BasicParser = require('../basicParser')
const { format } = require('editoria-i18n')

class RecommendationParser extends BasicParser {
  static apply(xmlDocument) {
    xmlDocument = RecommendationParser.addId(xmlDocument)
    const extractedData = RecommendationParser.extract(xmlDocument)
    return RecommendationParser.handleExtractedData(extractedData, xmlDocument)
  }

  static addId(xmlDocument) {
    const mentionedRecommendations = xmlDocument('recommandation')
    for (let i = 0; i < mentionedRecommendations.length; i++) {
      const recommendation = xmlDocument(mentionedRecommendations[i])
      recommendation.attr('id', `recommendation-${i}`)
    }
    return xmlDocument
  }

  static extract(xmlDocument) {
    const mentionedRecommendations = xmlDocument('recommandation')
    let recommendation
    const extractedData = []
    for (let i = 0; i < mentionedRecommendations.length; i++) {
      recommendation = xmlDocument(mentionedRecommendations[i]).clone()
      extractedData.push(recommendation)
    }
    return extractedData
  }

  static handleExtractedData(extractedData, xmlDocument) {
    const sectionId = 'recommendationSection'
    const sectionTitle = format('RECOMMENDATIONS')
    const place =
      xmlDocument('placeholder').length !== 0
        ? xmlDocument('placeholder:first-of-type')
        : xmlDocument('body')
    place.prepend(
      `<section id="${sectionId}" class="dedicatedPage">` +
        `<h1 class="unnumbered">${sectionTitle}</h1>` +
        `</section>`,
    )

    for (let i = 0; i < extractedData.length; i++) {
      const recommendation = xmlDocument(extractedData[i])
      const recommendationId = recommendation.attr('id')
      const recommendationContent = recommendation.text()
      xmlDocument(`#${sectionId}`).append(
        `<a href="#${recommendationId}"><p class="recommendationItem">${recommendationContent}</p></a>`,
      )
    }
    return xmlDocument
  }
}

module.exports = RecommendationParser
