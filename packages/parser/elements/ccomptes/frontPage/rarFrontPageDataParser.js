const BasicParser = require('../../basicParser')

class RarFrontPageDataParser extends BasicParser {
  static apply(xmlDocument) {
    const booktitle = this.getBookTitle(xmlDocument)
    return this.addFrontPageSection(xmlDocument, booktitle)
  }

  static getBookTitle(xmlDocument) {
    const bookTitle = xmlDocument('.booktitle').text()
    xmlDocument('.booktitle').remove()
    return bookTitle
  }

  static addFrontPageSection(xmlDocument, bookTitle) {
    xmlDocument('body').prepend(
      `<section class="titlepage">` +
        `<header>` +
        `<p class="report_type">observations définitives</p>` +
        `<p class="law_article">(Article R. 143-11 du code des juridictions financières)</p>` +
        `<h1 class="booktitle">${bookTitle}</h1>` +
        `</header>` +
        `<p class="subdisclaimer">Le présent document, qui a fait l'objet d'une contradiction avec les destinataires concernés, a été délibéré par la Cour des comptes, le JJ MM XXXX</p>` +
        `<p class="disclaimer">En application de l'article L. 143-1 du code des juridictions financières, la communication de ces observations est une prérogative de la Cour des comptes, qui a seule compétence pour arrêter la liste des destinataires</p>` +
        `<p class="footer">` +
        `<span class="adress">13, rue Cambon 75100 PARIS CEDEX 01</span>` +
        `<span class="phone_number">+33 1 42 98 95 00</span>` +
        `<span class="website_url">www.ccomptes.fr</span>` +
        `</p>` +
        `</section>`,
    )
    return xmlDocument
  }
}

module.exports = RarFrontPageDataParser
