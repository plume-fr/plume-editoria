const BasicParser = require('../../basicParser')

class RopFrontPageDataParser extends BasicParser {
  static apply(xmlDocument, bookType) {
    const booktitle = this.getBookTitle(xmlDocument)
    return this.addFrontPageSection(xmlDocument, booktitle, bookType)
  }

  static getBookTitle(xmlDocument) {
    const bookTitle = xmlDocument('.booktitle').text()
    xmlDocument('.booktitle').remove()
    return bookTitle
  }

  static addFrontPageSection(xmlDocument, bookTitle) {
    xmlDocument('body').prepend(
      `<section class="titlepage">` +
        `<header>` +
        `<p class="report_type">relevé d'observation provisoires</p>` +
        `<p class="law_article">(Article R. 143-7 du code des juridictions financières)</p>` +
        `<h1 class="booktitle">${bookTitle}</h1>` +
        `</header>` +
        `<p class="disclaimer">Destiné à recevoir les remarques des personnes destinataires, le présent document est provisoire et confidentiel.</p>` +
        `<p class="footer">` +
        `<span class="adress">13, rue Cambon 75100 PARIS CEDEX 01</span>` +
        `<span class="phone_number">+33 1 42 98 95 00</span>` +
        `<span class="website_url">www.ccomptes.fr</span>` +
        `</p>` +
        `</section>`,
    )
    return xmlDocument
  }
}

module.exports = RopFrontPageDataParser
