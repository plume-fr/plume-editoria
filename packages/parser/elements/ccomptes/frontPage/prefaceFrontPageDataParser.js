const BasicParser = require('../../basicParser')

class PrefaceFrontPageDataParser extends BasicParser {
  static apply(xmlDocument, bookType) {
    const booktitle = this.getBookTitle(xmlDocument)
    return this.addFrontPageSection(xmlDocument, booktitle, bookType)
  }

  static getBookTitle(xmlDocument) {
    return xmlDocument('.booktitle').text()
    // Get the title from the front page - do not remove it
  }

  static addFrontPageSection(xmlDocument, bookTitle) {
    xmlDocument('body').prepend(
      `<section class="prefaceTitlepage">` +
        '<div class="team">' +
        '<p class="chambre">[Numero] chambre</p>' +
        '<p class="section">[Numero] section</p>' +
        '<p class="people">Mme M</p>' +
        '</div>' +
        `<header>` +
        `<p class="report_type">Rapport d'analyse des réponses</p>` +
        `<h1 class="booktitle">${bookTitle}</h1>` +
        `</header>` +
        `<div class="disclaimer">` +
        `<p>Document confidentiel destiné aux membres de la collégialité délibérante</p>` +
        `<p>Déposé le [date]</p>` +
        `</div>` +
        `<p class="footer">` +
        `<span class="adress">13, rue Cambon 75100 PARIS CEDEX 01</span>` +
        `<span class="phone_number">+33 1 42 98 95 00</span>` +
        `<span class="website_url">www.ccomptes.fr</span>` +
        `</p>` +
        `</section>`,
    )
    return xmlDocument
  }
}

module.exports = PrefaceFrontPageDataParser
