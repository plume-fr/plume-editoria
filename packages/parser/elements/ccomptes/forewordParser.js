const BasicParser = require('../basicParser')
const { format } = require('editoria-i18n')

class ForewordParser extends BasicParser {
  static apply(xmlDocument) {
    const sectionTitle = format('FOREWORD')

    xmlDocument('body').prepend(
      `<section id="forewordSection" class="dedicatedPage">` +
        `<h1 class="unnumbered">${sectionTitle}</h1>` +
        `<div>` +
        `<p>En application des dispositions des articles L. 143-1 et L. 143-0-2 du code des juridictions financières, la Cour rend publiques ses observations et ses recommandations, au terme d’une procédure contradictoire qui permet aux représentants des organismes et des administrations contrôlées, aux autorités directement concernées, notamment si elles exercent une tutelle, ainsi qu’aux personnes éventuellement mises en cause de faire connaître leur analyse.</p>` +
        `<p>La divulgation prématurée, par quelque personne que ce soit, des présentes observations provisoires, qui conservent un caractère confidentiel jusqu’à l’achèvement de la procédure contradictoire, porterait atteinte à la bonne information des citoyens par la Cour. Elle exposerait en outre à des suites judiciaires l’auteur de toute divulgation dont la teneur mettrait en cause des personnes morales ou physiques ou porterait atteinte à un secret protégé par la loi.</p>` +
        `</div>` +
        `</section>`,
    )
    return xmlDocument
  }
}

module.exports = ForewordParser
