const BasicParser = require('../basicParser')

class CleanUpTagCCParser extends BasicParser {
  static apply (xmlDocument) {

        //  replace riop tag
    let changedXmlDoc = CleanUpTagCCParser.replace(xmlDocument, 'riop.titre-tableau', 'p', 'table-title')
    changedXmlDoc = CleanUpTagCCParser.replace(changedXmlDoc, 'riop.titre-graphique', 'p', 'chart-title')
    changedXmlDoc = CleanUpTagCCParser.replace(changedXmlDoc, 'riop.titre-carte', 'p', 'map-title')
    changedXmlDoc = CleanUpTagCCParser.replace(changedXmlDoc, 'riop.titre-schema', 'p', 'schema-title')
    changedXmlDoc = CleanUpTagCCParser.replace(changedXmlDoc, 'riop.source-tableau', 'p', 'table-source')
    changedXmlDoc = CleanUpTagCCParser.replace(changedXmlDoc, 'riop.observation', 'p', 'observation')


    return changedXmlDoc
  }
}

module.exports = CleanUpTagCCParser