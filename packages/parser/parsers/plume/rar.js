const { CComptesParser } = require('./ccomptes')
const ForewordParser = require('../../elements/ccomptes/forewordParser')
const RarFrontPageDataParser = require('../../elements/ccomptes/frontPage/rarFrontPageDataParser')
const PrefaceFrontPageDataParser = require('../../elements/ccomptes/frontPage/prefaceFrontPageDataParser')

function RARParser(xmlDocument, deletedRevisionsVisibles) {
  xmlDocument = CComptesParser(xmlDocument, deletedRevisionsVisibles)
  xmlDocument = ForewordParser.apply(xmlDocument)
  xmlDocument = RarFrontPageDataParser.apply(xmlDocument)
  xmlDocument = PrefaceFrontPageDataParser.apply(xmlDocument)
  return xmlDocument
}

module.exports = { RARParser }