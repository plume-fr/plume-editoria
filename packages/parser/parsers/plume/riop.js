const RopFrontPageDataParser = require('../../elements/ccomptes/frontPage/ropFrontPageDataParser')
const ForewordParser = require('../../elements/ccomptes/forewordParser')
const { CComptesParser } = require('./ccomptes')

function RIOPParser(xmlDocument, deletedRevisionsVisibles) {
  xmlDocument = CComptesParser(xmlDocument, deletedRevisionsVisibles)
  xmlDocument = ForewordParser.apply(xmlDocument)
  xmlDocument = RopFrontPageDataParser.apply(xmlDocument)
  return xmlDocument
}

module.exports = { RIOPParser }