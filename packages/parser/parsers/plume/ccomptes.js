const RecommendationParser = require('../../elements/ccomptes/recommendationParser')
const CleanUpTagParser = require('../../elements/cleanUpTagParser')
const CleanUpTagCCParser = require('../../elements/ccomptes/cleanUpTagCCParser')
const TableOfContentParser = require('../../elements/tableOfContentParser')
const FootNoteParser = require('../../elements/footnoteParser')
const RevisionParser = require('../../elements/revisionParser')

function CComptesParser(xmlDocument, deletedRevisionsVisibles) {
  xmlDocument = RevisionParser.apply(xmlDocument, deletedRevisionsVisibles)
  xmlDocument = CleanUpTagParser.apply(xmlDocument)
  xmlDocument = CleanUpTagCCParser.apply(xmlDocument)
  xmlDocument = FootNoteParser.apply(xmlDocument)
  xmlDocument = RecommendationParser.apply(xmlDocument)
  xmlDocument = TableOfContentParser.apply(xmlDocument)
  return xmlDocument
}

module.exports = { CComptesParser }