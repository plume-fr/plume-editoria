const RodFrontPageDataParser = require('../../elements/ccomptes/frontPage/rodFrontPageDataParser')
const { CComptesParser } = require('./ccomptes')

function RODParser(xmlDocument, deletedRevisionsVisibles) {
  xmlDocument = CComptesParser(xmlDocument, deletedRevisionsVisibles)
  xmlDocument = RodFrontPageDataParser.apply(xmlDocument)
  return xmlDocument
}

module.exports = { RODParser }