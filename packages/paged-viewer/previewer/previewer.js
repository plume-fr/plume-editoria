let ready = new Promise(function (resolve, reject) {
    if (document.readyState === "interactive" || document.readyState === "complete") {
        resolve(document.readyState);
        return;
    }

    document.onreadystatechange = function ($) {
        if (document.readyState === "interactive") {
            resolve(document.readyState);
        }
    }
});

ready.then(async function () {
    // Create a new Previewer
    let previewer = new Paged.Previewer();

    // Get the URL to load
    let params = URLSearchParams && new URLSearchParams(document.location.search.substring(1));
    let url = params && params.get("url") && decodeURIComponent(params.get("url"));

    if (!url) {
        console.error("No 'url' parameter given.");
        return;
    }

    // Fetch and Parse Contents
    let html = await fetch(url)
        .then(response => response.text())
        .then(str => (new DOMParser()).parseFromString(str, "text/html"))

    // Gather all stylesheets from html document
    let hrefs = previewer.removeStyles(html);

    // Add a stylesheet url in params
    let stylesheet = params && params.get("stylesheet") && decodeURIComponent(params.get("stylesheet"));
    if (stylesheet) {
        hrefs.push(stylesheet);
    }

    // Push all body elements to a document fragment
    let content = html.querySelectorAll('body > *');


    let fragment = document.createDocumentFragment();
    for (let i = 0; i < content.length; i++) {
        fragment.appendChild(content[i]);
    }

    // Run the Paged Preview
    let done = await previewer.preview(fragment, hrefs, document.body);
});