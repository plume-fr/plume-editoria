# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-data-model@0.1.1...editoria-data-model@0.1.2) (2019-05-28)




**Note:** Version bump only for package editoria-data-model

<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-data-model@0.1.0...editoria-data-model@0.1.1) (2019-04-25)


### Bug Fixes

* publication date fix ([88cbfea](https://gitlab.coko.foundation/editoria/editoria/commit/88cbfea))




<a name="0.1.0"></a>
# 0.1.0 (2019-04-12)


### Bug Fixes

* **authsome:** lint problem ([7891594](https://gitlab.coko.foundation/editoria/editoria/commit/7891594))
* **data-model:** make tests run again ([b30372d](https://gitlab.coko.foundation/editoria/editoria/commit/b30372d))


### Features

* add first version of models with input validation ([b4adea3](https://gitlab.coko.foundation/editoria/editoria/commit/b4adea3))
* **app:** Given name and surname now supported in Signup ([745ab2e](https://gitlab.coko.foundation/editoria/editoria/commit/745ab2e)), closes [#197](https://gitlab.coko.foundation/editoria/editoria/issues/197)
* **authorize:** update rules in certain events fo the user ([89ee01e](https://gitlab.coko.foundation/editoria/editoria/commit/89ee01e))
* **dashboard:** full name for user used in sorting ([64ec470](https://gitlab.coko.foundation/editoria/editoria/commit/64ec470))
* **data model:** add lock model ([a22d473](https://gitlab.coko.foundation/editoria/editoria/commit/a22d473))
* **data-model:** working book, division & component models ([3b2db01](https://gitlab.coko.foundation/editoria/editoria/commit/3b2db01))
* **dataloader:** create a data loader ([f5b12a8](https://gitlab.coko.foundation/editoria/editoria/commit/f5b12a8))
