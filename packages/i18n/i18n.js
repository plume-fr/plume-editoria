const { IntlMessageFormat } = require('intl-messageformat')
const frFR = require('./locale/fr-FR.json')
const enUS = require('./locale/en-US.json')
const config = require('config')

const defaultLocale = config.i18n ? config.i18n.locale : 'en-US'

function format(id, values, locale) {
  locale = typeof locale !== 'undefined' ? locale : defaultLocale
  const MESSAGES = {
    'fr-FR': frFR,
    'en-US': enUS,
  }
  return MESSAGES[locale][id]
    ? new IntlMessageFormat(MESSAGES[locale][id], locale).format(values)
    : id
}

module.exports = { format }
