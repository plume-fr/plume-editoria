const { format } = require('../i18n')

test('i18n for french', () => {
  const locale = 'fr-FR'
  const formatted = format('HELLO', { name: 'Gulliver' }, locale)
  expect(formatted).toEqual('bonjour Gulliver')
})

test('i18n for english', () => {
  const locale = 'en-US'
  expect(format('BYE', {}, locale)).toEqual('bye')
})

test('missing translation ', () => {
  const locale = 'en-US'
  expect(format('404_TRADUCTION_NOT_FOUND', {}, locale)).toEqual('404_TRADUCTION_NOT_FOUND')
})