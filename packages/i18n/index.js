const { format } = require('./i18n')
const frFR = require('./locale/fr-FR.json')
const enUS = require('./locale/en-US.json')

module.exports = {
  format,
  frFR,
  enUS,
}
