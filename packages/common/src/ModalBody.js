import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

const Body = styled.div`
  flex-grow: 1;
  font-family: ${th('fontInterface')};
  overflow: auto;
  padding: 16px;
`

export default Body
