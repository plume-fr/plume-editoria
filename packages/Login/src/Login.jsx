import React from 'react'
import { Redirect } from 'react-router'
import PropTypes from 'prop-types'
import { Field } from 'formik'
import { isEmpty } from 'lodash'
import { override } from '@pubsweet/ui-toolkit'
import {
  CenteredColumn,
  ErrorText,
  H1,
  Link,
  Button,
  TextField,
} from '@pubsweet/ui'

import styled from 'styled-components'

const Logo = styled.div`
  ${override('Login.Logo')};
`
Logo.displayName = 'Logo'

const FormContainer = styled.div`
  ${override('Login.FormContainer')};
`

const UsernameInput = props => (
  <TextField label="Identifiant" placeholder="identifiant" {...props.field} />
)
const PasswordInput = props => (
  <TextField
    label="Mot de passe"
    placeholder="mot de passe"
    {...props.field}
    type="password"
  />
)

const Login = ({
  errors,
  handleSubmit,
  logo = null,
  signup = true,
  passwordReset = true,
  redirectLink,
}) =>
  redirectLink ? (
    <Redirect to={redirectLink} />
  ) : (
    <div>
      {logo && (
        <Logo>
          <img
            alt="ccomptes-logo"
            src="https://upload.wikimedia.org/wikipedia/fr/thumb/3/39/Logo_Cours_des_comptes_%28France%29.svg/1920px-Logo_Cours_des_comptes_%28France%29.svg.png"
          />
        </Logo>
      )}
      <FormContainer>
        <div className="form">
          {!isEmpty(errors) && <ErrorText>{errors}</ErrorText>}
          <form onSubmit={handleSubmit}>
            <Field component={UsernameInput} name="username" />
            <Field component={PasswordInput} name="password" />
            <Button primary type="submit">
              Se connecter
            </Button>
          </form>

          {signup && (
            <div>
              <Link to="/signup">Créer un compte</Link>
            </div>
          )}
          <div className="link-to-blog">
            <Link to="https://eig-plume.gitlab.io/_pages/avancement">
              Nouveautés du produit
            </Link>
          </div>
        </div>
      </FormContainer>
    </div>
  )

Login.propTypes = {
  error: PropTypes.string,
  actions: PropTypes.object,
  location: PropTypes.object,
  signup: PropTypes.bool,
  passwordReset: PropTypes.bool,
  logo: PropTypes.string,
}

export default Login
