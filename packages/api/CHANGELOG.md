# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.6"></a>
## [0.1.6](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.1.5...editoria-api@0.1.6) (2019-05-28)




**Note:** Version bump only for package editoria-api

<a name="0.1.5"></a>
## [0.1.5](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.1.4...editoria-api@0.1.5) (2019-05-22)


### Bug Fixes

* **signup:** fix permission rule when new user created ([bea7622](https://gitlab.coko.foundation/editoria/editoria/commit/bea7622))




<a name="0.1.4"></a>
## [0.1.4](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.1.3...editoria-api@0.1.4) (2019-04-25)


### Bug Fixes

* publication date fix ([88cbfea](https://gitlab.coko.foundation/editoria/editoria/commit/88cbfea))




<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.1.2...editoria-api@0.1.3) (2019-04-25)


### Bug Fixes

* more fixes ([f6028f5](https://gitlab.coko.foundation/editoria/editoria/commit/f6028f5))




<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.1.1...editoria-api@0.1.2) (2019-04-24)


### Bug Fixes

* **authorize:**  can fragment edit rule  foreach bookcomponent ([7a6c4c0](https://gitlab.coko.foundation/editoria/editoria/commit/7a6c4c0))
* **dashboard:** sorting fix for capital letters ([c09939f](https://gitlab.coko.foundation/editoria/editoria/commit/c09939f))
* **globalproduction:** add to team prodctionEditor non admin user ([c08e32f](https://gitlab.coko.foundation/editoria/editoria/commit/c08e32f))
* **reorder:** wrapper queries into one transaction ([10ab3e3](https://gitlab.coko.foundation/editoria/editoria/commit/10ab3e3))
* **trackchange:** get updates of the workflow and trackchange ([c0b0b3b](https://gitlab.coko.foundation/editoria/editoria/commit/c0b0b3b))
* more UI fixes ([78e14da](https://gitlab.coko.foundation/editoria/editoria/commit/78e14da))




<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/editoria/editoria/compare/editoria-api@0.1.0...editoria-api@0.1.1) (2019-04-12)


### Bug Fixes

* **editoria-api:** path fix ([3e62108](https://gitlab.coko.foundation/editoria/editoria/commit/3e62108))




<a name="0.1.0"></a>
# 0.1.0 (2019-04-12)


### Bug Fixes

* constants export and pubsweet server import ([399229a](https://gitlab.coko.foundation/editoria/editoria/commit/399229a))
* **authsome:** add athsome rules to resolvers ([1305074](https://gitlab.coko.foundation/editoria/editoria/commit/1305074))
* **authsome:** chagne query not get deleted ([d72044f](https://gitlab.coko.foundation/editoria/editoria/commit/d72044f))
* **authsome:** fix subscription for authosme updates ([1ce1fd9](https://gitlab.coko.foundation/editoria/editoria/commit/1ce1fd9))
* **authsome:** move team rules to bookbuilder ([0cab675](https://gitlab.coko.foundation/editoria/editoria/commit/0cab675))
* **globalteam:** migrate new tables for the globalteam page ([d8d0d37](https://gitlab.coko.foundation/editoria/editoria/commit/d8d0d37))
* **team:** clear unneeded imports ([77070b9](https://gitlab.coko.foundation/editoria/editoria/commit/77070b9))
* **teammanager:** teamanger add bug as search ([631c579](https://gitlab.coko.foundation/editoria/editoria/commit/631c579))


### Features

* **dashboard:** wire up refetch ui with graphql ([aa99cef](https://gitlab.coko.foundation/editoria/editoria/commit/aa99cef))
* bootstrap graphql ([8028771](https://gitlab.coko.foundation/editoria/editoria/commit/8028771))
* subscriptions in progress ([2d92222](https://gitlab.coko.foundation/editoria/editoria/commit/2d92222))
* **app:** Given name and surname now supported in Signup ([745ab2e](https://gitlab.coko.foundation/editoria/editoria/commit/745ab2e)), closes [#197](https://gitlab.coko.foundation/editoria/editoria/issues/197)
* **authorize:** update rules in certain events fo the user ([89ee01e](https://gitlab.coko.foundation/editoria/editoria/commit/89ee01e))
* **authsome:** add subscriptions to get updates ([c042093](https://gitlab.coko.foundation/editoria/editoria/commit/c042093))
* **authsome:** move authorize to backend ([4e00def](https://gitlab.coko.foundation/editoria/editoria/commit/4e00def))
* **bookbuilder:** drag and drop ([bd21f36](https://gitlab.coko.foundation/editoria/editoria/commit/bd21f36))
* **dashboard:** archive books and user's full name support ([8bd0a25](https://gitlab.coko.foundation/editoria/editoria/commit/8bd0a25)), closes [#226](https://gitlab.coko.foundation/editoria/editoria/issues/226) [#197](https://gitlab.coko.foundation/editoria/editoria/issues/197)
* **dashboard:** sorting books in dashboard ([d22c27e](https://gitlab.coko.foundation/editoria/editoria/commit/d22c27e))
* **globalteam:** migrate globalTeams to graphql ([2c6f870](https://gitlab.coko.foundation/editoria/editoria/commit/2c6f870))
* **teams:** migrate team to graphql ([cc161e9](https://gitlab.coko.foundation/editoria/editoria/commit/cc161e9))
