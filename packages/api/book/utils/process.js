const cheerio = require('cheerio')

module.exports = ({
  styles,
  activeConverters,
  book,
  notesPart,
  recommandationsPart,
  previewer,
}) => bookComponent => {
  const $ = cheerio.load(bookComponent.content)
  const fragmentTitle = bookComponent.title || 'Untitled'
  const fragmentId = bookComponent.id
  const bookTitle = book.title
  const fragmentDivision = bookComponent.division
  const fragmentSubcategory = bookComponent.componentType
  const fragmentNumber = Object.prototype.hasOwnProperty.call(
    bookComponent,
    'number',
  )
    ? bookComponent.number
    : -1

  if (notesPart === undefined) {
    activeConverters.forEach(converter =>
      converter(
        $,
        fragmentId,
        fragmentTitle,
        bookTitle,
        fragmentDivision,
        fragmentSubcategory,
        fragmentNumber,
        recommandationsPart,
      ),
    )
  } else {
    activeConverters.forEach(converter =>
      converter(
        $,
        fragmentId,
        fragmentTitle,
        bookTitle,
        fragmentDivision,
        fragmentSubcategory,
        fragmentNumber,
        notesPart,
        recommandationsPart,
      ),
    )
  }
  styles.forEach(uri => {
    $('<link rel="stylesheet"/>')
      .attr('href', uri)
      .appendTo('head')
  })

  const content = $.html()
  return {
    title: fragmentTitle,
    id: fragmentId,
    content,
    division: fragmentDivision,
    type: fragmentSubcategory,
  }
}
