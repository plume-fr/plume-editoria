const config = require('config')
const findIndex = require('lodash/findIndex')
const forEach = require('lodash/forEach')
const fs = require('fs')
const groupBy = require('lodash/groupBy')
const HTMLEPUB = require('html-epub')
const map = require('lodash/map')
const output = require('./output')
const { parse } = require('plume-book-parser')
const pullAll = require('lodash/pullAll')

const {
  BookTranslation,
  BookComponent,
  BookComponentTranslation,
  Division,
} = require('editoria-data-model/src').models

const divisionTypeMapper = {
  Frontmatter: 'front',
  Body: 'body',
  Backmatter: 'back',
}
const EpubBackend = async (
  bookId,
  destination,
  clientConverter,
  clientPreviewer,
  style,
) => {
  try {
    // Apply CSS
    // TODO: to be decided where the per applications themes should live
    const stylesRoot = `${__dirname}/themes`
    let fontsRoot =
      config.epub && config.epub.fontsPath
        ? process.cwd() + config.epub.fontsPath
        : null

    if (!fs.existsSync(fontsRoot)) fontsRoot = ''

    // TODO: read the path to the uploads folder from config
    const resourceRoot = `${process.cwd()}/uploads`

    const previewer = clientPreviewer || 'vivliostyle'
    const bookTranslation = await BookTranslation.query()
      .where('bookId', bookId)
      .andWhere('languageIso', 'en')
    const book = {
      title: bookTranslation[0].title,
      identifier: bookId,
    }

    let bookContent = await getBookContent(bookId)
    bookContent = `<span class="booktitle">${book.title}</span>${bookContent}`
    let bookStyle = style.split('.css')[0]
    const convertedBook = parse(bookContent, bookStyle)

    let outcome
    switch (previewer) {
      default: {
        outcome = new HTMLEPUB(book, { resourceRoot, stylesRoot, fontsRoot })
        await outcome.load([convertedBook])
        break
      }
      case 'paged': {
        outcome = await createHTMLDocument(
          book,
          convertedBook,
          resourceRoot,
          `${stylesRoot}/${style}`,
          fontsRoot,
        )
        break
      }
    }

    if (destination === 'folder') {
      return output.folder(outcome, stylesRoot, previewer, style)
    }
    return output.attachment(outcome, bookId)
  } catch (e) {
    throw new Error(e)
  }
}

const cheerio = require('cheerio')

const getBookContent = async bookId => {
  // chapters
  const bookComponentIds = []
  const divisions = await Division.query()
    .where('bookId', bookId)
    .andWhere('deleted', false)

  forEach(divisions, division => {
    const { bookComponents } = division
    forEach(bookComponents, id => {
      bookComponentIds.push(id)
    })
  })

  // Get book parts
  const bookParts = await Promise.all(
    map(bookComponentIds, async (bookComponentId, i) => {
      const bookComponent = await BookComponent.findById(bookComponentId)
      const bookComponentTranslation = await BookComponentTranslation.query()
        .where('bookComponentId', bookComponent.id)
        .andWhere('languageIso', 'en')
      const division = await Division.findById(bookComponent.divisionId)
      const sortedPerDivision = await Promise.all(
        map(division.bookComponents, async id => {
          const bookComponent = await BookComponent.query()
            .where('id', id)
            .andWhere('deleted', false)
          return bookComponent[0]
        }),
      )

      const groupedByType = groupBy(
        pullAll(sortedPerDivision, [undefined]),
        'componentType',
      )

      const componentTypeNumber =
        findIndex(
          groupedByType[bookComponent.componentType],
          item => item.id === bookComponent.id,
        ) + 1

      return {
        id: bookComponent.id,
        content: bookComponentTranslation[0].content,
        title: bookComponentTranslation[0].title,
        componentType: bookComponent.componentType,
        division: divisionTypeMapper[division.label],
        number: componentTypeNumber,
      }
    }),
  )

  // Merge parts to create a unique book
  let bookContent = ''
  let bookPart, parsedPart
  for (let i = 0; i < bookParts.length; i++) {
    bookPart = bookParts[i]
    parsedPart =
      `<content attr-id='${bookPart.id}'` +
      `attr-title='${bookPart.title}'` +
      `attr-componentType='${bookPart.componentType}'` +
      `attr-division='${bookPart.division}'` +
      `attr-number='${bookPart.number}'` +
      `>
          ${bookPart.content}
        </content>`
    bookContent += parsedPart
  }
  return bookContent
}

const createHTMLDocument = async (
  book,
  content,
  resourceRoot,
  stylesRoot,
  fontsRoot,
) => {
  const output = cheerio.load(
    `<!DOCTYPE html>
        <html>
        <head>
        <title>${book.title}</title>
        <meta charset="UTF-8"><link rel="stylesheet" href="${stylesRoot}"/>
        </head>
        <body class="hyphenate" lang="en-us">
        </body>
        </html>`,
  )

  output('body').append(content)
  output('img').each((index, img) => {
    const $img = output(img)

    const tempUri = $img.attr('src')
    $img.attr('src', `/var/www/plume/${tempUri}`)
  })
  return output
}

module.exports = EpubBackend
