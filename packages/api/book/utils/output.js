const logger = require('@pubsweet/logger')
const fs = require('fs')
const crypto = require('crypto')
const mkdirp = require('mkdirp')
const unzipper = require('unzipper')
const { exec } = require('child_process')

const attachment = async (epub, id) => {
  const path = `${process.cwd()}/uploads/collection-${id}.epub`
  const wstream = fs.createWriteStream(path)

  try {
    const archive = await epub.stream(wstream)
    return new Promise((resolve, reject) => {
      archive.on('error', err => {
        throw new Error(err)
      })

      archive.on('end', () => {
        logger.info('Wrote %d bytes', archive.pointer())
        resolve(`/uploads/collection-${id}.epub`)
      })
    })
  } catch (error) {
    throw new Error(error)
  }
}

const writeFile = (location, content) =>
  new Promise((resolve, reject) => {
    fs.writeFile(location, content, 'utf8', err => {
      if (err) return reject(err)
      return resolve()
    })
  })
const readFile = location =>
  new Promise((resolve, reject) => {
    fs.readFile(location, 'utf8', (err, data) => {
      if (err) return reject(err)
      return resolve(data)
    })
  })

const folder = async (outcome, stylesRoot = undefined, previewer, style) => {
  // TODO: read the path to the uploads folder from config
  const folder = `${previewer}/${crypto.randomBytes(32).toString('hex')}`
  const path = `${process.cwd()}/uploads/${folder}`
  if (fs.existsSync(path)) {
    throw new Error('Output path already exists')
  }

  mkdirp.sync(path)

  if (previewer === 'vivliostyle') {
    try {
      const archive = await outcome.stream(unzipper.Extract({ path }))
      return new Promise((resolve, reject) => {
        archive.on('error', err => {
          throw new Error(err)
        })

        archive.on('end', () => {
          logger.info('Wrote %d bytes', archive.pointer())
          resolve(folder)
        })
      })
    } catch (error) {
      throw new Error(error)
    }
  } else {
    try {
      // We will exec paged js command in order to export the pdf

      const where = `${path}/index.html`
      const styles = `${path}/default.css`
      const content = outcome.html()
      const stylesContent = await readFile(`${stylesRoot}/${style}`)
      const pathImg = `${process.cwd()}/uploads`.replace(/\//g, '\\/')
      await writeFile(where, content)
      await writeFile(styles, stylesContent)
      const changePath = await exec(
        `sed 's/\\/uploads/${pathImg}/' ${path}/index.html >> ${path}/index2.html`,
        (error, stdout, stderr) => {
          if (error) {
            return 'error'
          }
          console.log(`stdout: ${stdout}`)
          console.log(`stderr: ${stderr}`)
          console.log(
            `~/pagedjs-cli/bin/paged -i ${path}/index2.html -o ${path}/index.pdf --additional-script ${pagedJsScriptPath}`,
          )
        },
      )
      const pagedJsScriptPath = `${__dirname}/pagedJsScripts/*`
      const pdfExport = await exec(
        `~/pagedjs-cli/bin/paged -i ${path}/index2.html -o ${path}/index.pdf --additional-script ${pagedJsScriptPath}`,
        (error, stdout, stderr) => {
          if (error) {
            console.error(`exec error: ${error}`)
            return 'error'
          }
          console.log(`stdout: ${stdout}`)
          console.log(`stderr: ${stderr}`)
        },
      )
      return folder
    } catch (error) {
      console.log(error)
      return 'error'
    }
  }
}

module.exports = {
  attachment,
  folder,
}
