# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.0.1"></a>
## [1.0.1](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-vivliostyle-viewer@1.0.0...pubsweet-component-vivliostyle-viewer@1.0.1) (2018-11-20)




**Note:** Version bump only for package pubsweet-component-vivliostyle-viewer
