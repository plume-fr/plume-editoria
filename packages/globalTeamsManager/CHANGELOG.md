# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.2.3"></a>
## [0.2.3](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.2.2...pubsweet-component-editoria-global-teams@0.2.3) (2019-05-28)




**Note:** Version bump only for package pubsweet-component-editoria-global-teams

<a name="0.2.2"></a>
## [0.2.2](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.2.1...pubsweet-component-editoria-global-teams@0.2.2) (2019-05-17)




**Note:** Version bump only for package pubsweet-component-editoria-global-teams

<a name="0.2.1"></a>
## [0.2.1](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.2.0...pubsweet-component-editoria-global-teams@0.2.1) (2019-04-24)




**Note:** Version bump only for package pubsweet-component-editoria-global-teams

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.1.2...pubsweet-component-editoria-global-teams@0.2.0) (2019-04-12)


### Bug Fixes

* **authsome:** add missing file ([5847772](https://gitlab.coko.foundation/editoria/editoria/commit/5847772))
* **globalteam:** migrate new tables for the globalteam page ([d8d0d37](https://gitlab.coko.foundation/editoria/editoria/commit/d8d0d37))
* **upgrade:** remove redux dependencies upgrade client ([be4d87b](https://gitlab.coko.foundation/editoria/editoria/commit/be4d87b))


### Features

* **authsome:** add subscriptions to get updates ([c042093](https://gitlab.coko.foundation/editoria/editoria/commit/c042093))
* **globalteam:** migrate globalTeams to graphql ([2c6f870](https://gitlab.coko.foundation/editoria/editoria/commit/2c6f870))
* **teams:** migrate team to graphql ([cc161e9](https://gitlab.coko.foundation/editoria/editoria/commit/cc161e9))




<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-editoria-global-teams@0.1.1...pubsweet-component-editoria-global-teams@0.1.2) (2018-11-20)




**Note:** Version bump only for package pubsweet-component-editoria-global-teams
